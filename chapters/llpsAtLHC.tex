%!TEX root = ../dissertation.tex
\chapter{Long-lived Particle Basics }
\label{chp:llpIntro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This chapter provides some helpful information and formulas related to long-lived particles. It includes a discussion of how to compute a particle's lifetime, mean decay length, and the probability for an individual particle to decay at a particular distance. 

%The mean decay length of a particle at the \ac{LHC} is related to its boost, while decay position of any individual particle is sampled from an exponential. 

%\subsection{\ac{SM} Phenomenology at the \ac{LHC}}
%The \ac{LHC} circulates protons. Collisions are between the proton's constituents, quarks and gluons. Proton consists of two up quarks and one down quark (\textit{valance quarks}), held together by gluons. Also \textit{sea quarks}. Most interested in the fraction of proton's energy. 
\section{Particle Width and Lifetime}

A particle's lifetime is inversely proportional to its total decay rate $\Gamma$, or \textit{width},

\begin{equation}
\tau = \frac{1}{\Gamma_{\textrm{Total}}}.
\label{eqn:particle_liftime}
\end{equation}

A particle's width is the probability per unit time that the particle will decay. Particles often decay via several different channels. The \textit{partial decay width} for a given decay mode can be thought of as the probability per unit time that a particle will decay via that particular channel. In the case a particle can decay via several modes, the particle's total width can be computed as

\begin{equation}
\Gamma_{\textrm{Total}} = \sum_{i=1}^{n} \Gamma_i.
\end{equation}

For a given process, the partial decay width can be calculated using Fermi's golden rule~\cite{LEE2019}

\begin{equation}
\Gamma_i = \frac{1}{2m_X} \int{}{} d\Pi_{f} | \Mu (m_X \rightarrow \{p_f\} ) |^{2}. 
\label{eqn:particle_partial_width}
\end{equation}

The mass of the particle is given by $m_X$, $\Mu$ is the \textit{matrix element} for the particle's decay into the decay products $\{p_f\}$, and $d\Pi_{f}$ is the allowed Lorentz-invariant phase space for the decay. The above formulas use $\hbar = c = 1$.

The matrix element quantifies the transformation from the initial particle to its decay products, and can be computed for a given process using Feynman rules. Generally, the matrix element includes terms for the initial particle, any intermediate particles in the process, as well as the outgoing particles. Couplings which describe the strength of the interactions between particles also contribute to the matrix element. 

Stable particles have infinitely narrow widths, while particles with very short lifetimes have wider widths and will appear as resonances around their nominal mass. 

In the Standard Model, particles have lifetimes which span several orders of magnitudes. Figure~\ref{fig:sm_particle_mass_v_lifetime} shows the lifetime and mass of several Standard Model particles. Shaded regions roughly indicate which particles are considered prompt, and which are considered stable to a general purpose detector (assuming particles are moving at the speed of light, $\beta=1$). 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Theory/SMParticlesLifetime.png}
    \caption[Lifetime and mass of Standard Model Particles]{A selection of \ac{SM} particles are shown as a function of their mass and proper lifetime. Shaded regions roughly represent the detector-prompt and detector-stable regions of lifetime space, for a particle with $\beta=1$~\cite{LEE2019}.}\label{fig:sm_particle_mass_v_lifetime}
  \end{center}
\end{figure}

From equations~\ref{eqn:particle_partial_width} and~\ref{eqn:particle_liftime}, a particle can obtain a long-lifetime if there is limited phase-space available for the decay or if there is a small matrix element. A decay process can have a small matrix element for a variety of reasons, including decays which involve small couplings, as well as a decay which proceeds via highly virtual intermediate particles.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mean Decay Length}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

At the \ac{LHC}, a particle's lifetime can be inferred by measuring the three-dimensional distance between the point at which it was produced, and the point at which it decays. This quantity is known as the decay length, $\Delta L$. A particle's mean decay length is defined as

\begin{equation}
\langle \Delta L \rangle = \frac{v}{c} \gamma \tau  = \beta \gamma \tau
\label{eqn:decayLength} 
\end{equation}
where $\beta \gamma$ is the ratio of the particle's momentum to its mass $\frac{p}{m}$, and $\tau$ is the particle's lifetime in its rest frame. 

Particles at the \ac{LHC} are produced with a large range of momenta. Reconstructible particles have momenta which range from $\approx 1$ GeV to momenta as high as $\approx 1$ TeV. Table~\ref{tab:particledecaylengths} shows the average decay length for several Standard Model particles produced at the \ac{LHC}. Electrically charged and electrically neutral particles are shown separately. Particles with longer $\langle \Delta L \rangle$ are shown first. 

The first group of particles look stable to a general purpose tracking detector (i.e have mean decay lengths greater than approximately $1$ meter). If these stable, or meta-stable particles are are electrically charged, it is possible to reconstruct their trajectory as a track. The middle group of particles decay within the detector, a measurable distance away from the point at which they were produced. The last group of particles decay so quickly that it is only possible to detect their decay products. 

\begin{table}[!htbp]
\begin{center}
\caption[Particle lifetime, mass, and mean decay length]{ Particle lifetime, mass, and average decay length (if the particle's momentum $p=1$ ~GeV and $=100$ GeV). }\label{tab:particledecaylengths}
\begin{adjustbox}{max width=0.75\textwidth}
\begin{tabular}{ c  c  c  c}

\hline	
\hline	
\textbf{Charged Particle} & Lifetime [s] & $\langle \Delta L \rangle$ at 1 GeV  & $\langle \Delta L \rangle$ at 100 GeV   \\ 
\hline
$e^{\pm}$ 	& $\infty$    			& $\infty$ 			& $\infty$ \\ 
$p^{\pm}$ 	& $\infty$    			& $\infty$ 			& $\infty$ \\ 
$\mu^{\pm}$ & $2.2\cdot10^{-06}$  	& 6.2 km 			& 620 km   \\
$\pi^{\pm}$ & $2.6\cdot10^{-08}$   	& 56 m 				& 5.6 km   \\ 
$K^{\pm}$   & $1.2\cdot10^{-08}$ 	& 7.5 m      		& 750 m    \\
\hline	
$\Sigma^{\pm}$,$\Xi^{\pm}$,$\Omega^{\pm}$  & $\sim1.2\cdot10^{-10}$ & $\sim3$ cm & $\sim3$ m \\
$D^{\pm}$ & $1.0\cdot10^{-12}$ 		& 170 \textmu m   	& 17 mm  \\
$B^{\pm}$ & $1.6\cdot10^{-12}$ 		& 93 \textmu m    	& 9.3 mm \\
$\tau$    & $2.9\cdot10^{-13}$  	& 49 \textmu m   	& 4.9 mm \\ 
\hline			
W  		  & $3\cdot10^{-25} $		& $\ll$ nm 			& $\ll$ nm \\ 
t 		  & $5\cdot10^{-25}$		& $\ll$ nm 			& $\ll$ nm \\ 
\hline
\hline
\textbf{Neutral Particle} & Lifetime [s] & $\langle \Delta L \rangle$ at 1 GeV  & $\langle \Delta L \rangle$ at 100 GeV   \\ 
\hline
$\gamma$ 	& $\infty$    			& $\infty$ 				& $\infty$ \\ 
$\nu$ 		& $\infty$    			& $\infty$ 				& $\infty$ \\ 
$n$			& 881					& $2.8\cdot10^{8}$ km 	& $2.8\cdot10^{10}$ km \\
$K_{L}$   	& $5.116\cdot10^{-08}$  & 31 m 				 	& 3.1 km \\
\hline	
$\Lambda^{0}$,$\Sigma^{0}$,$\Xi^{0}$ & $\sim2.2\cdot10^{-10}$ 	& $\sim7$ cm & $\sim7$ m \\
$K_{S}$   		& $9.0\cdot10^{-11}$  	 	 & 5.4 cm				& 5.4 m  \\
$B^{0}$   		& $1.5\cdot10^{-12}$ 	     & 86 \textmu m   		& 8.6 mm \\
$D^{0}$   		& $4.1\cdot10^{-13}$ 	     & 65 \textmu m   		& 6.5 mm \\
\hline	
$\pi^{0}$ & $8.5\cdot10^{-17} $  	 		& 0.19 \textmu m 		& 19 \textmu m  \\ 
Z  		  & $3\cdot10^{-25} $		 		& $\ll$ nm 			& $\ll$ nm   \\ 
H  		  & $1.56\cdot10^{-22} $	 		& $\ll$ nm 			& $\ll$ nm   \\ 
\hline
\hline
\end{tabular}
\end{adjustbox}
\end{center}
\end{table}

In \ac{ATLAS}, the decay length can be measured directly by reconstructing a \ac{DV} at the position where long-lived particle decayed. Reconstructing a displaced vertex involves reconstructing the trajectories of a particle's decay products. These decay products will form a vertex at the point where their trajectories intersect, which directly corresponds to the point in space where the long-lived particle decayed. The distance between the displaced vertex and the point where the long-lived particle was produced, indicated by a \ac{PV}, is equal to the decay length.

The decay length can also be inferred by measuring the \textit{impact parameter} of the decay products. If the decay products have electric charge, their trajectories may be reconstructed as tracks. The impact parameter of a track is then defined as the distance of closest approach of the track to the primary vertex.

Figure~\ref{fig:decayLength} shows a schematic of a long-lived particle decaying to several charged daughter particles. The primary vertex is where the long-lived particle is produced, and the displaced vertex is where the long-lived particle decays. The distance between the primary vertex and displaced vertex is the decay length, whereas the impact parameter for a single displaced track is shown. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/Theory/DecayLength.png}
    \caption[Schematic of a long-lived particle decay]{A Schematic of a long-lived particle decay is shown. The long-lived particle is produced at the primary vertex (blue), and decays at the location of a displaced vertex (pink), which can be reconstructed from the electrically charged daughter particles (solid black lines). The three dimensional decay length $\Delta L$ (dotted black line) of the long-lived particle is indicated, as well as the impact parameter (orange) of one of the daughter particles produced in the decay (thick solid black line).}\label{fig:decayLength}
  \end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exponential Decay}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The probability a particle survives for a time $t$ before it decays is described by an exponential. At the \ac{LHC}, particles are rarely produced at rest, and the most interesting quantity to measure is the distance traveled by the particle inside the detector before it decays. For a particle with a given $\beta \gamma$, the probability a particle survives for a distance $L$ is given by,

\begin{equation}
P(L) = e^{-L/(\beta \gamma \tau)},
\label{eqn:lifetimeExponential}
\end{equation}
Where $\tau$ is the particle's mean lifetime as measured in the particle's rest-frame.  

Figure~\ref{fig:llp_exponentials} shows some example distributions of the probability that a particle decays at a particular decay length $L$, for a particle with a mean proper lifetime of $\tau=1$~ns and several possible values of $\beta\gamma$. These distributions are shown for $L$ up to $10$~m, the typical scale of a general purpose detector. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/Theory/THESIS_big_lifetime_ATLAS_lin.png}
    \caption[Decay length distributions for a long-lived particle with $\tau=1$~ns]{Decay length distributions for a long-lived particle with $\tau=1$~ns. Decay length distributions are shown for several possible values of $\beta\gamma$. Vertical dotted lines indicate useful dimensions of a general purpose detector. The vertical dotted line at $1.1$~m indicates the typical separation between the tracking detector and calorimeter, and the vertical dotted line at $4.2$~m indicates the typical separation between the calorimeter and muon spectrometer. The figure extends to $10$~m, the usual outer edge of a muon spectrometer.}\label{fig:llp_exponentials}
  \end{center}
\end{figure}

For particles with the same momentum, $\beta\gamma$ is smaller for particles with larger masses. Particles with larger $\beta\gamma$, or longer lifetimes, will have larger mean decay lengths than particles with less boost, or shorter lifetimes.

Typical \ac{LHC} experiments look for evidence of a long-lived particle decay inside a single sub-detector. Because the decay position of long-lived particles are described by an exponential, the sensitivity of any search depends on the sub-detector volume and position. For detectors of a fixed volume, the sensitivity improves as the distance to the primary vertex is decreased. For detectors with a fixed position, the sensitivity improves as the volume is increased. 

%The probability of a particle decaying inside different regions of a general purpose detector is discussed in Appendix~\ref{AppendixA}, for a model with pair-production of long-lived particles. Searches which only require evidence of a single long-lived particle decay have better acceptance than searches which require two long-lived particle decays. Searches which look for a single long-lived particle decay inside the tracking detector also have the better acceptance than searches which look for decays in other sub-detectors, even though the tracking detector is typically small in volume. This result is due to the fact that any individual particle's decay position is sampled from an exponential distribution, and most decays occur close to the $pp$-interaction point. 

%earches which look for a single particle decay inside the Inner Detector have acceptance greater than $1\%$ to the widest range of particle lifetimes, between $\tau=10^{-3}$~ns and $\tau=10^{2}$~ns. 

%Because any individual particle's decay position is sampled from an exponential distribution, searches which look for evidence of long-lived particle decays close to the $pp$-interaction point have strong sensitivity over a wide range of lifetimes. 