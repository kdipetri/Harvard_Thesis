\chapter{Background Estimation}
\label{chp:backgroundEstimation}

%!TEX root = ANA-SUSY-2018-33-INT1.tex


As discussed in the previous section, there are three major processes which result in reconstructed displaced muons that can contribute to this analysis: cosmic muons, algorithm fakes, and muons from Standard Model processes, such as in-flight decays of pions, kaons, and heavy-flavor hadrons.

Events which have a preselected muon from a cosmic ray event, algorithm fake, or decay in flight may also have a high-mass and high-$\nTracks$ displaced vertex. The processes which result in a high mass and high $\nTracks$ displaced vertex are expected to be unrelated to the process which resulted in a displaced muon. The three major sources of multi-track displaced vertices are described below.
\begin{itemize}
 \item \emph{Hadronic interactions}: Particles which interact with the detector also produce secondary vertices. These secondary vertices can have high invariant mass and have many associated tracks, but most often occur in regions of material. The contribution of hadronic interactions is drastically reduced by the material veto.
 \item \emph{Randomly crossing tracks}: A random track which crosses a low-mass, low-$\nTracks$ displaced vertex might be reconstructed as part of the vertex. Reconstructing a vertex with an additional track both promotes the vertex to a higher track multiplicity and increases the mass of the vertex, especially if the randomly crossing track crosses the displaced vertex at a large angle.
 \item \emph{Merged vertices}: One key step in secondary vertex reconstruction is to merge vertices which are nearby. If two low-mass, and low-$\nTracks$ vertices from different sources are merged, a single high-mass, high-$\nTracks$ vertex is formed.
\end{itemize}

A \textit{transfer factor} inspired method is used to estimate background contributions from cosmic muons, fake muons, and muons from heavy-flavor decays to the final signal regions. This technique hinges on the assumption that the features of the muon used to veto these background muons are uncorrelated with the properties of displaced vertices reconstructed in the event. This assumption is validated by comparing background predictions to observed data in dedicated validation regions. %A future appendix will also include dedicated studies showing that the variables used in muon vetoes are not correlated with displaced-vertex properties.

The following sections begin by providing an overview of the method used to estimate contributions from muon backgrounds. This overview is followed by a detailed description of the estimation for each background, as well as validations performed to ensure the method performs as expected.

\section{Overview of Control, Validation, and Signal Regions}\label{sec:CR_VR_overview}

A schematic which describes the various muon selections and displaced-vertex selections used in the background estimation can be found in Figure~\ref{fig:dvmuon_CR_VR_overview}. Two axes are defined, which represent properties of the leading preselected muon ($y$-axis) and the displaced vertices in the event ($x$-axis).
%The cosmic control region is defined by events in which the leading preselected muon passes the fake-muon veto and isolation requirement. At this point, events should be dominated by cosmics. The x-axis is then separated into two regions. The left side of the x-axis is defined by events in which the leading preselected muon fails the cosmic veto, while the right side of the x-axis includes muons which pass pass the cosmic veto, i.e. the final muon selection.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/DVMuonRegionOverview}
    \caption{Skematic of event selections used to study background processes}\label{fig:dvmuon_CR_VR_overview}
  \end{center}
\end{figure}

The $y$-axis defines four exclusive categories of events based on the leading preselected muon in the event. These categories include three \ac{CR}s, and one region where muons satisfy the full muon selection.
\begin{itemize}
 \item Heavy-Flavor \ac{CR}: Events in which the leading preselected muon passes all criteria in the full muon selection, except for isolation. The isolation requirement is inverted, and this category is dominated by events with muons from Standard Model processes such as pion, kaon, and heavy-flavor decays.
 \item Fake-Muon \ac{CR}: Events in which the leading preselected muon passes all criteria in the full muon selection, except for the veto for fake muons. The fake muon veto is inverted, and this category is dominated by events with algorithm fake muons. 
 \item Cosmic \ac{CR}: Events in which the leading preselected muon passes the full muon selection, except for the cosmic veto. The cosmic veto is inverted, and this category is dominated by events with cosmic muons.
 \item Full Selection: Events in which the leading preselected muon passes the full muon selection. This category is most sensitive to signal.
\end{itemize}

The $x$-axis defines six exclusive categories of events based on the displaced vertices in an event. Each subsequent category is increasingly sensitive to displaced vertices from signal. The first three categories are collectively referred to as the Displaced Vertex Control Region (\ac{DV} \ac{CR}). The next two categories are regions used to validate the background estimation method, or \ac{VR}s, and the final category includes events which have at least one displaced vertex that passes the full selection. The final category also serves as a \ac{SR}.
\begin{itemize}
 \item Vertex~\ac{CR}: Events with no preselected displaced vertices. This category is expected to be dominated by background. This control region can be further separated into three subcategories, which will be useful for evaluating systematic uncertainties associated with the transfer factor measurement.
 \begin{itemize}
 	\item $0$~\ac{DV}s: Events with no vertices satisfying $R_{xy}<300$ mm, $|Z|<300$ mm, $\chi^2/N_{DoF}<5$, and $4$ mm from any \ac{PV} in the event.
 	\item Mat.~2~trk: Events with at least one displaced vertex located inside material, as indicated by the material map, that has exactly $2$ tracks.
 	\item Mat.~3~trk: Events with at least one displaced vertex located inside material, as indicated by the material map, that has $\geq3$ tracks.
 \end{itemize}
 \item \ac{VR}~low~$\nTracks$: Events with a displaced vertex that passes preselection, in which the highest track multiplicity vertex has $\nTracks = 2$.
 \item \ac{VR}~low~$\mDV$: Events with a displaced vertex that passes preselection, and has at least 3 tracks and $\mDV<20$ GeV. No displaced vertices in the event pass the full displaced vertex selection.
 \item Vertex~\ac{SR}: Events which have at least one displaced vertex passing the full selection. This includes all preselection requirements, $\nTracks \geq 3$ and $\mDV>20$ GeV
\end{itemize}

\section{Overview of Transfer Factor Method}\label{sec:ABCD_overview}

The general idea behind the transfer factor background estimation method is summarized below, using the cosmic background as an example. Four regions are defined, labeled A, B, C, and D. Regions A and B have \textit{signal-like} displaced vertices, while C and D have no preselected displaced vertices. Regions A and C have \textit{signal-like} muons, while B, and D have \textit{cosmic-like} muons. These regions are labeled in Figure~\ref{fig:example_ABCD_for_cosmics}, and described in more detail below.
\begin{itemize}
 \item A: Events in which the leading preselected muon passes the full muon selection, and at least one displaced vertex passes the full \ac{DV} selection. This region corresponds to the bottom right square in Figure~\ref{fig:dvmuon_CR_VR_overview}.
 \item B: Events in which the leading preselected muon passes all criteria in the full muon selection, but fails the cosmic veto. The event also has at least one displaced vertex which pases the full \ac{DV} selection. This region corresponds to events in the Cosmic \ac{CR} and Vertex \ac{SR}.
 \item C: Events in which the leading preselected muon passes the full muon selection, but has no displaced vertices which pass preselection. This region correspond to events in the muon Full~Selection category and the Vertex~\ac{CR}.
 \item D: Events in which the leading preselected muon passes all criteria in the full muon selection, but fails the cosmic veto. Events in this region also have no displaced vertices which pass preselection. Events in this category correspond to the Cosmic~\ac{CR} and Vertex \ac{CR}.
\end{itemize}

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/ExampleABCDMethod}
    \caption[Schematic of the transfer factor background estimation for cosmic background]{Schematic of the transfer factor background estimation for cosmic background. Important regions are indicated in blue and labeled in white. The transfer factor is measured using event counts in Regions C and D (solid black line), and applied to Region B to predict the background contribution to Region A (dotted line). }\label{fig:example_ABCD_for_cosmics}
  \end{center}
\end{figure}

The first step in this background estimation is to measure a transfer factor. In the case of the cosmic background, this transfer factor, TF, estimates the ratio of cosmic muons which pass the cosmic veto, to that of cosmic muons which which fail the cosmic veto (TF $= N_C/N_D$). This transfer factor must be measured in a region which is pure in the background of interest, and expected to be free of events from signal. Because events in Regions C and D do not have any displaced vertices which pass preselection, they should be sufficiently free of signal to measure such a transfer factor.

Once the transfer factor is measured, it can be used to estimate the expected contribution of cosmic events to the final signal region. One key assumption in the this background estimation method is that the ratio of cosmic muons which pass the cosmic veto to those which fail the veto does not change as a function of the displaced vertex properties of the event. This assumption means the ratio $N_A/N_B$ should be equivalent to $N_C/N_D$ for cosmic muons.

If this assumption holds true, the transfer factor can be applied to events which have a cosmic-like muon, but a signal-like displaced vertex (i.e. Region B), in order to estimate the number of cosmic events in Region A. The expected cosmic muon contribution to the final Signal Region is defined as $N_A = N_B \cdot N_C/N_D$.

This method can be validated by applying the same transfer factor to regions with displaced vertices that are consistent with background, and have a cosmic-like muon.

For completeness, the total expected background in the signal region $A$ in sample $s$ is:

\begin{eqnarray}
N_A[s] = \sum_k{N_{B_k}[s] \times \frac{N_{C_k}^{k,s^\prime(k)} }{N_{D_k}^{k,s^\prime(k) } } }
\end{eqnarray}

\noindent where $N$ is the number of events, $k$ is {cosmics, fakes, or isolation}, $s^\prime$ differs based on the particular background component, and regions are denoted by $A, B, C, D$.

\section{Transfer Factors}\label{sec:transfer_factors}

The first step in the background estimation method is finding regions in which to measure the associated transfer factors. These regions must be dominated by a single background. One natural way to separate backgrounds is to use the $\MET$ and the Muon triggered samples as two separate regions. However, there are three transfer factors which must be measured, and only two different samples.

Cosmic muons are the dominant background in the Muon triggered sample, with a sub-dominant contribution of heavy-flavor muons, and a negligible contribution of fake muons. Muons from algorithm fakes and heavy-flavor decays dominate the $\MET$ selection, where cosmic muons are a negligible background. Thus far, it is clear that events without any preselected displaced vertices in the Muon triggered sample can be used to measure the cosmic transfer factor, while the $\MET$ triggered sample can be used used to measure fake and heavy-flavor transfer factors.

It is possible to use the muon's transverse impact parameter to separate muons from heavy-flavor decays and fakes. Figure~\ref{fig:muon_d0_heavyflavor_v_fakes} shows the transverse impact parameter, $|d_0|$, for muons in the Heavy-Flavor \ac{CR} and Fake-Muon \ac{CR} of the $\MET$ triggered sample. Events are required to have no preselected displaced vertices. Muons from heavy-flavor decays tend to have smaller transverse impact parameters, with a tail that extends to a few millimeters, which is expected given the lifetime of $b$-hadrons. Muons which are reconstructed from algorithm fakes have a relatively flat transverse impact parameter distribution. This effect is expected because algorithm fakes are combined muons reconstructed with fake Large Radius Tracks, which tend to produce a flat distribution in $|d_0|$. 

%Muons from heavy-flavor processes are primarily produced in $b$-hadron decays, which have lifetimes on the order of $\tau \approx 1.5$ ps. At the \ac{LHC}, $b$-hadrons typically result in decays which occur on average $1~\micron$ to approximately $10$ mm away from the primary vertex. In order to produce a muon with $\pT > 25$ GeV, the $b$-hadron must be sufficiently boosted, and may decay on the farther end of that scale. However, the boost of the $b$-hadron means that the resulting muon will at most have a transverse impact parameter on the order of a few milimeters.

%Muons which are reconstructed from algorithm fakes have a relatively flat transverse impact parameter distribution. This effect is expected because algorithm fakes are combined muons reconstructed with fake Large Radius Tracks, which tend to produce a flat distribution in $|d_0|$.

%Figure~\ref{fig:muon_d0_heavyflavor_v_fakes} shows the transverse impact parameter, $|d_0|$, for muons in the Heavy-Flavor and Fake-Muon \ac{CR}s of the $\MET$ triggered sample. Events are required to not have any preselected displaced vertices (i.e. be in the Displaced Vertex \ac{CR}). Fake-Muon \ac{CR} muons fail the veto for fake muons but pass the cosmic veto and isolation. In order to increase the statistics available for studying heavy-flavor muons, the muon preselection is slightly modified. Preselected muons are still required to have $\pT > 25$ GeV, and $|\eta|<2.5$, but the $|d_0|>2.0$ mm requirement is reduced to $|d_0|>1.5$~mm. Heavy-Flavor \ac{CR} muons fail isolation but pass the fake and cosmic veto.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/BackgroundEstimation/Compared0_METTrig_mu_absd0_log.png}
    \includegraphics[width=0.45\textwidth]{plots/BackgroundEstimation/Compared0_METTrig_mu_absd0zoom_lin.png}
    \caption[Muon $|d_0|$ in Fake-Muon and Heavy-Flavor Control Regions]{Transverse impact parameter for muons in the $\MET$ triggered sample Fake-Muon and Heavy-Flavor Control Regions. The minimum impact parameter is relaxed from $|d_0|>2.0$ mm to $|d_0|>1.5$ mm in order to increase the statistics available for measuring the Heavy-Flavor Transfer Factor. Events are required to have no preselected displaced vertices. }\label{fig:muon_d0_heavyflavor_v_fakes}
  \end{center}
\end{figure}

The following requirements were designed to separate fake-muons and muons from heavy-flavor decays. Heavy-flavor muons selected by requiring muons have $1.5 < |d_0| <3$ mm. In contrast, fake muons are selected by requiring $|d_0|>5$ mm. A similar requirement on the muon transverse impact parameter, of $|d_0|>5$ mm, is also applied to the muons when measuring the cosmic muon transfer factor. This requirement ensures that muons from heavy-flavor decays do not contribute to the cosmic transfer factor measurement.

\subsection{Cosmic Transfer Factor}

As described above, the cosmic transfer factor is evaluated in the Muon triggered sample, using muons which pass the following selections.
\begin{itemize}
 \item Selection applied to all muons: leading preselected muon with $\pT>62$ GeV, $|\eta|<1.05$, $|d_0|>5.0$ mm, trigger matched, pass fake-muon veto, pass isolation
 \item Numerator: muon pass cosmic veto
 \item Denominator: muons rejected by cosmic veto
\end{itemize}

Events used to evaluate the nominal cosmic transfer factor must also have zero preselected displaced vertices. The nominal transfer factor is evaluated using events in the displaced vertex \ac{CR}. In order to evaluate a systematic uncertainty, the transfer factor is separately evaluated using the $0$~\ac{DV} and the Mat.~2~trk displaced vertex categories and compared to the nominal estimate. From these two separate measurements, the largest variation from the nominal transfer factor is taken as a systematic designed to account for possible correlations of the transfer factor with displaced vertex properties. The Mat.~3~trk region is not considered for evaluating a systematic uncertainty, because the heavy-flavor background is predicted to dominate this region by orthogonal background estimates.

An additional source of uncertainty is evaluated to account for the assumption that a transfer factor evaluated with muons that have $|d_0|>5.0$ mm, can be extrapolated to muons with smaller $|d_0|$. This uncertainty is estimated by evaluating the transfer factor separately for muons with $5<|d_0|<100$ mm and $100<|d_0|<300$ mm. Events with zero preselected displaced vertices are used to evaluate these two transfer factors. The binning used to evaluate these two transfer factors was optimized to minimize the statistical uncertainty on each measurement. From these two separate measurements, the largest variation from the nominal transfer factor is taken as a systematic.

The two separate sources of systematic uncertainty (variations of \ac{DV} requirements and variations of muon requirements) are assumed to be uncorrelated, and added in quadrature to estimate the total systematic uncertainty.

\subsection{Fake Transfer Factor}

The fake transfer factor is evaluated in the $\MET$ triggered sample, using muons which pass the following selections.
\begin{itemize}
 \item Selection applied to all muons: leading preselected muon with $\pT>25$ GeV, $|\eta|<2.5$, $|d_0|>5.0$ mm, pass cosmic veto, pass isolation
 \item Numerator: muons pass fake-muon veto
 \item Denominator: muons rejected by fake-muon veto
\end{itemize}

Events used to evaluate the nominal fake-muon transfer factor must also have zero preselected displaced vertices. The nominal transfer factor is evaluated using events in the displaced vertex \ac{CR}. In order to evaluate a systematic uncertainty, the transfer factor is separately evaluated using the three displaced vertex \ac{CR} sub-regions, and compared to the nominal estimate. From these separate measurements, the largest variation from the nominal transfer factor is taken as the systematic uncertainty. This uncertainty is designed to account for any possible correlation of the fake-muon transfer factor with properties of vertices in the event. Events with more collision activity could result in more fake muons and more displaced vertices from random crossings or hadronic interactions.

An additional source of uncertainty is evaluated to account for the assumption that a transfer factor evaluated with muons that have $|d_0|>5.0$ mm can be extrapolated to muons with smaller $|d_0|$. This uncertainty is estimated by evaluating the transfer factor separately for muons with $5<|d_0|<100$ mm and $100<|d_0|<300$ mm. Events with zero preselected displaced vertices are used to evaluate these two transfer factors. The binning used to evaluate these two transfer factors was optimized to minimize the statistical uncertainty on each measurement. From these two separate measurements, the largest variation from the nominal transfer factor is taken as a systematic.

The two separate sources of systematic uncertainty (variations of \ac{DV} requirements and variations of muon requirements) are assumed to be uncorrelated, and added in quadrature to estimate the total systematic uncertainty.

\subsection{Heavy Flavor Transfer Factor}

The heavy-flavor transfer factor is evaluated in the $\MET$ triggered sample, using muons which pass the following selections.
\begin{itemize}
 \item Selection applied to all muons: leading preselected muon with $\pT>25$ GeV, $|\eta|<2.5$, $1.5<|d_0|<3.0$ mm, pass fake-muon veto, pass cosmic veto
 \item Numerator: muons pass isolation criteria
 \item Denominator: muons rejected by isolation criteria
\end{itemize}

Events used to evaluate the nominal heavy-flavor transfer factor must also have zero preselected displaced vertices. The nominal transfer factor is evaluated using events in displaced vertex \ac{CR}. In order to evaluate a systematic uncertainty, the transfer factor is separately evaluated using the three different displaced vertex \ac{CR} sub-regions, and compared to the nominal estimate. This uncertainty is designed to account for any possible correlation of the heavy-flavor transfer factor with properties of vertices in the event. From these separate measurements, the largest variation from the nominal transfer factor is taken as the systematic uncertainty.

A second source of uncertainty is evaluated to account for the assumption that a transfer factor evaluated with muons that have $1.5<|d_0|<3.0$ mm can be extrapolated to muons with larger $|d_0|$. This uncertainty is estimated by evaluating the transfer factor separately for muons with $1.5<|d_0|<1.7$ mm and $1.7<|d_0|<3.0$ mm. Events without any preselected displaced vertices are used to evaluate these two transfer factors. As was done in the fake and cosmic transfer factor measurements, the binning used to evaluate these two transfer factors was optimized to minimize the statistical uncertainty on each measurement. From these two separate measurements, the largest variation from the nominal transfer factor is taken as a systematic.

The two separate sources of systematic uncertainty (variations of \ac{DV} requirements and variations of muon requirements) are assumed to be uncorrelated, and added in quadrature to estimate the total systematic uncertainty.

\subsection{Transfer factor results}

Table~\ref{tab:transfer_factors} shows the measured transfer factors for the cosmic, fake-muon, and heavy-flavor backgrounds. The nominal transfer factor is shown for each background, along with the statistical and systematic uncertainty of each measurement.

\begin{table}[!htb]
\begin{center}
\caption{Measured transfer factors }\label{tab:transfer_factors}
 \begin{tabular}{cc}

\hline\hline
\input{plots/BackgroundEstimation/CosmicTFs.tex}
\hline
\input{plots/BackgroundEstimation/FakeTFs.tex}
\hline
\input{plots/BackgroundEstimation/HeavyFlavorTFs.tex}
\hline\hline

 \end{tabular}
\end{center}
\end{table}

Cosmic muons have the smallest transfer factor, followed by fake muons, and muons from heavy-flavor decays. The cosmic veto and fake-muon veto do an excellent job of rejecting each background to less than $1\%$ of their original values. In contrast, nearly $10\%$ of heavy-flavor muons pass the isolation requirement. %As described above, the Mat.~3~trk region is excluded from the normalization here to avoid contributions from heavy-flavor backgrounds.

Because cosmic muons are the largest background for this analysis, the cosmic transfer factor has the smallest statistical uncertainty. The cosmic transfer factor has a total systematic uncertainty of $12\%$. In contrast, the fake-muon transfer factor has a statistical uncertainty of approximately $30 \%$ and a systematic uncertainty of approximately $50 \%$. The heavy-flavor transfer factor has a statistical uncertainty of roughly $20 \%$, and a systematic uncertainty of approximately $35 \%$.
\tabularnewline

\section{Validation of Background Estimation}\label{sec:background_validation}

These transfer factors are then used to estimate the number of expected events from each background to the Signal Region. In order to validate the performance of this method, background predictions are made for all six displaced vertex categories, and these predictions are compared to the observed number of events in data. 

\subsection{$\MET$ selection Results}

Table~\ref{tab:background_met_controlregions} shows the number of events observed in each muon event category and the six displaced vertex categories for the $\MET$ triggered sample.

\begin{table}[!htb]
\begin{center}
 \caption{$\MET$ triggered sample Control Regions yields}\label{tab:background_met_controlregions}
 \begin{tabular}{ccccc}
  \hline\hline
\input{plots/BackgroundEstimation/CR_MET_data_observed.tex}
  \hline\hline
  \end{tabular}
 \end{center}
\end{table}

The number of observed events in the three muon Control Regions are then scaled by the relevant transfer factor. Table~\ref{tab:background_met_validationregions} shows the predicted contribution of each muon background to the six displaced vertex categories. The total predicted background is compared to the observed number of events in data.

\begin{table}[!htb]
\begin{center}
 \caption{Background Estimation for $\MET$ Triggered Sample}\label{tab:background_met_validationregions}
 \begin{tabular}{cccc}
  \hline\hline
\input{plots/BackgroundEstimation/VR_MET_data_predictions.tex}
  \hline\hline
  \end{tabular}
 \end{center}
\end{table}

In the $\MET$ triggered sample, the total background is expected to be less than one event. The dominant background is due to muons from heavy-flavor decays, followed by a subdominant contribution of fake muons. The contribution of cosmic muons to the $\MET$ sample is expected to be negligible. The total statistical uncertainty on the background is $37\%$, while the systematic uncertainty is $37\%$. The total background prediction for the $\MET$ triggered Signal Region is $0.43\pm0.16\pm0.16$ events.

Figure~\ref{fig:validationregions_metstream} shows the number of predicted events in the $\MET$ Triggered sample, separated by Validation Region. Good agreement between the predicted and observed number of events is observed within uncertainties.

%Figure
\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{plots/BackgroundEstimation/fig_04a}
    \caption[$\MET$ trigged sample Validation Region predicted and observed events]{Background predicted and observed events in $\MET$ triggered sample Validation Regions are shown in the top panel. The bottom panel shows the ratio of observed events to the total background prediction. The error band shows the total uncertainty of the background prediction, which includes the statistical and systematic uncertainties added in quadrature~\cite{ATLAS-CONF-2019-006}. }
    \label{fig:validationregions_metstream}
  \end{center}
\end{figure}

\subsection{Muon Triggered Sample Results}

Table~\ref{tab:background_muon_controlregions} shows the number of events observed in each muon event category and the six displaced vertex categories for the Muon selection.

\begin{table}[!htb]
\begin{center}
 \caption{Muon triggered sample Control Regions yields}\label{tab:background_muon_controlregions}
 \begin{tabular}{ccccc}
  \hline\hline
\input{plots/BackgroundEstimation/CR_Muon_data_observed.tex}
  \hline\hline
  \end{tabular}
 \end{center}
\end{table}

The number of observed events in each control region is then scaled by the relevant transfer factor. Table~\ref{tab:background_muon_validationregions} shows the predicted contribution of each muon background, to the six displaced vertex categories. The total predicted background is compared to the observed number of events in data. The total background prediction for the Muon selection Signal Region is $1.88\pm0.20\pm0.28$ events.

\begin{table}[!htb]
\begin{center}
 \caption{Background Estimation for Muon Triggered Sample }\label{tab:background_muon_validationregions}
 \begin{tabular}{cccc}
  \hline\hline
\input{plots/BackgroundEstimation/VR_Muon_data_predictions.tex}
  \hline\hline
  \end{tabular}
 \end{center}
\end{table}

Figure~\ref{fig:validationregions_muonstream} also shows the number of predicted events in the Muon triggered sample, separated by displaced vertex category. Good agreement between the predicted and observed number of events is observed within statistical uncertainties.

%Figure
\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{plots/BackgroundEstimation/fig_04b}
    \caption[Muon trigged sample Validation Region predicted and observed events]{Background predictions and observed events in Muon triggered sample Validation Regions are shown in the top panel. The bottom panel shows the ratio of observed events to the total background prediction. The error band shows the total uncertainty of the background prediction, which includes the statistical and systematic uncertainties added in quadrature~\cite{ATLAS-CONF-2019-006}.}
    \label{fig:validationregions_muonstream}
  \end{center}
\end{figure}

In the Muon triggered sample Signal Region, the total background is expected to be nearly two events. The dominant background is from cosmic muons, followed by a sub-dominant contribution of heavy-flavor muons. The contribution of fake muons to the Muon triggered sample is negligible in comparison. The statistical uncertainty on the total predicted background is $10\%$ while the systematic uncertainty is $14\%$.

%\clearpage
%
%\section{Systematic uncertainty on background estimate}\label{sec:background_systematics}
%
%{\TODO I think larry wrote this, put in own words}
%
%As described above, the systematic uncertainties on the background are derived by comparing the transfer factor values as measured in the %nominal control regions to the values obtained in various sub-regions of the control regions. While the inclusive control regions are used to %retain statistical power, slices of these \ac{CR} in \ac{DV}-properties and muon-properties are also used to measure transfer factors. The %size of the variation within the nominal \ac{CR} is taken as a systematic uncertainty on the transfer factor itself. As this is a fully data-%driven background estimate, these variations should account for any residual correlation between the \ac{DV} and muon properties. These %measured variations can be found in Table~\ref{tab:transfer_factors}.
%
%For the final Signal Regions, this method gives a systematic uncertainty of $0.16/0.43=37\%$ for the $\MET$ triggered \ac{SR} and $0.28/1.94=14%.4\%$ for the Muon triggered \ac{SR} as measured across \ac{DV} requirements of (no DVs, 2-track DVs in material regions, 3-track DVs in %material regions) and muon requirements (low and high $d_0$ regions described above). 





