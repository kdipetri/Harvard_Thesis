\chapter{Data and Monte Carlo Samples}
\label{chp:dataMC}

%{\color{red} Mostly done except for a few data distributions}
This chapter describes the data and Monte Carlo samples which are used to perform the analysis described in Part~\ref{part:search}. The dataset, and details of how the data are processed with non-standard reconstruction, is described in Section~\ref{sec:data}. Monte Carlo samples are generated for the benchmark signal model discussed in Chapter~\ref{chp:theory}. These simulated signals are used to optimize signal region definitions and estimate the sensitivity of the analysis. The steps required to produce these samples are complicated, and described in Section~\ref{sec:monteCarlo}. Kinematic properties of these samples are presented in Section~\ref{sec:rhadKinematics}.

\section{Data}\label{sec:data}

This analysis makes use of \ac{LHC} $pp$-collision data at $\sqrt{s}=13$ TeV collected by the \ac{ATLAS} between 2016 and 2018. This dataset corresponds to an integrated luminosity of $136~\ifb$. The uncertainty on the total integrated luminosity is $1.7\%$. The mean $\langle \mu \rangle$ is approximately $33$ interactions per bunch crossing, and the peak $\langle \mu \rangle$ is approximately $70$ interactions per bunch crossing. 

Data are collected with a Muon Spectrometer only trigger and a missing transverse energy trigger, both of which are described in greater detail in Section~\ref{chp:trigger}. Non-standard reconstruction methods are used to improve sensitivity to long-lived particle decays that result in displaced tracks and displaced vertices. These reconstruction methods are computationally intensive, and require a complicated data-flow, which is described in the following section. Background processes which result in displaced muons and displaced vertices are also estimated from data. 
%Forward reference to non-standard reconstruction, and the data-flow described in the following section.

\subsection{Data-flow}\label{sec:drawRPVLLStream}

The Large Radius Tracking and secondary vertex reconstruction used in this analysis are both computationally expensive. In order to reduce the computational resources required for event reconstruction, only a subset of the the full \verb=physics_Main= dataset is reconstructed with Large Radius Tracking and secondary vertex reconstruction. 

The subset of events which are processed with non-standard reconstruction are selected with requirements designed for specific long-lived particle analyses in \ac{ATLAS}. Events are selected with \textit{filters} when standard reconstruction is run over the entire \verb=physics_Main= event stream at Tier-$0$. These filters are unique to each long-lived particle analysis, and include selections with basic trigger and kinematic requirements, applied to standard physics objects. Criteria are designed to select events that are interesting for the long-lived particle analysis in mind. In total, $5-10\%$ of the full \verb=physics_Main= dataset is selected for non-standard reconstruction. 

Figure~\ref{fig:drawRVLLrates} shows the rate of events selected by long-lived particle filters for a representative 2018 run. The total rate of events selected is shown in black dots, while the rate of events selected by individual filters is indicated with a separate color. The total rate is less than the stacked histograms because the same event may be selected by multiple filters. Rates are shown as a function of \textit{luminosity-block} and instantaneous luminosity. A \textit{lumi-block} corresponds to roughly $60$ seconds of data taking. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{plots/DRAW_RPVLL/run_358333_Rate.png}
    %\includegraphics[width=0.58\textwidth]{plots/DRAW_RPVLL/run_358333_totalRate.png}
    %\includegraphics[width=0.38\textwidth]{plots/DRAW_RPVLL/run_358333_totalrateVsLumi.png}
    \caption[Long-lived particle filter rates]{ Left: The rate of events selected by long-lived particle filters for a representative 2018 run, shown as a function of lumi-block. The total rate (Overall RPVLL rate) is shown in black dots, while the rate of each filter is shown with a separate color. Right: The total rate of events selected by long-lived particle filters is shown as a function of instantaneous luminosity.}
    \label{fig:drawRVLLrates}
  \end{center}
\end{figure}

It is challenging to define a filter selection which results in good signal efficiency and allows for data-driven background estimations without exceeding rate limitations. The filtering step is not aware of the quality requirements and calibrations usually placed on physics objects in an \ac{ATLAS} analysis. These quality criteria are usually defined by performance groups after filtering has already taken place. Selections placed on leptons and jets must therefore be quite loose, and are not always robust to pile-up. As a result, the rate of events saved increases exponentially with pile-up. 

Events which pass a long-lived particle filter are saved in the \texttt{RAW} format (i.e. detector signals), as \DRAWRPVLL~datasets. It is necessary to save events as detector-level data because these special reconstruction methods require access to low-level information, such as Inner Detector hits. At a later time, the data are processed with non-standard reconstruction methods, and events are saved in an analysis object format. 

Another challenge for long-lived particle analyses is that the standard reconstruction used in the filtering step does not always produce the same physics objects as the non-standard reconstruction. An event reconstructed with standard methods might result in a muon with a different Inner Detector track than if the event were reconstructed with Large Radius Tracking. 

This analysis uses events selected by two filters. The first filter aims to select events collected by the Muon Spectrometer only trigger described in Chapter~\ref{chp:trigger}. The second aims to collect events which pass a missing transverse energy trigger.

The muon filter first requires events pass the \verb=HLT_mu60_0eta105_msonly= trigger. Events are also required to contain at least one muon, which satisfies the following, year dependent, criteria. 
\begin{itemize}
  \item \textbf{2016:} The event must have a least one offline muon with $\pT>60$ GeV and $|\eta| < 2.5 $. If the offline muon is combined, and the \ac{ID} and \ac{MS} tracks have $\chi^{2}/N_{DoF} < 5$, the muon's Inner Detector track must also have $|d_0| > 1.5$ mm. If the \ac{ID} and \ac{MS} match have a $\chi^{2}/N_{DoF} > 5$, then no $d_0$ requirement is applied. No impact parameter requirements are applied if the muon is not combined. The different treatment for combined muons which have a poor $\chi^{2}/N_{DoF}$ match is due to the fact that filtering is performed on events without Large Radius Tracking. During the filtering step, in which nominal reconstruction is run, a very displaced signal muon could have it's Muon Spectrometer track incorrectly matched to the nearest prompt \ac{ID} track. The incorrect \ac{ID} track would fail a $d_0$ cut. To maintain signal efficiency, no $d_0$ requirement is applied if the $\chi^{2}/N_{DoF}$ indicates a poor ID-MS track match. The optimization of this requirement is described in more detail in this Internal Note~\cite{ATL-COM-PHYS-2017-1060}
  \item \textbf{2017 and 2018:} The event must have a least one offline muon with $\pT>60$ GeV and $|\eta| < 2.5 $. No requirement is made on the muon's transverse impact parameter. 
\end{itemize}

The $\met$ filter for the 2016 dataset related to this search required any of the following triggers:
\begin{itemize}
\setlength\itemsep{0em}
  \item \verb=HLT_xeXX=
  \item \verb=HLT_xeXX_tc_lcw=
  \item \verb=HLT_xeXX_mht_L1XEYY=
  \item \verb=HLT_xe110_mht_L1XE50_AND_xe70_L1XE50=
  \item \verb=HLT_xeXX_topoclPS=
  \item \verb=HLT_xeXX_topoclPUC=
\end{itemize}
where \texttt{XX} = 90, 100, 110, 120, 130 and \texttt{YY} = 50, 55, 60.
In 2017, the $\met$ filter used the following triggers in addition to those used for 2016.
\begin{itemize}
\setlength\itemsep{0em}
  \item \verb=HLT_xe110_pufit_L1XE60=
  \item \verb=HLT_xe120_pufit_L1XE60=
\end{itemize}
In 2018, the $\met$ filter used the following trigger in addition to those used for 2016 and 2017.
\begin{itemize}
\setlength\itemsep{0em}
  \item \verb=HLT_xe110_pufit_xe70_L1XE50=
\end{itemize}
%where \texttt{ZZ} = 50, 55, 60, 70.

In addition to the trigger requirement, events are also required to have cluster-based $\MET$ > $180~\mathrm{GeV}$, where the cluster-based $\MET$ computation is described in Section~\ref{sec:caloclusters}. This requirement is used to reduce the rate of events passing the filter.

Usually, there is only once chance to apply the filter to each run. The 2015 data were not filtered with triggers of interest to this analysis. For this reason, this analysis only uses 2016-2018 data. 

Events which have passed the filters described above are reconstructed with Large Radius Tracking and secondary vertex reconstruction and saved as \DAODRPVLL~datasets. These \DAODRPVLL~datasets are then further processed into \DAODSUSY~derivations. All events which pass the triggers discussed above enter the \DAODSUSY~derivations, but requirements are placed on the analysis level objects in those events in order to reduce the content of each event saved. For example Muon Spectrometer hits are not saved in derivations, and only Inner Detector tracks with $\pT>1$~GeV are saved. The \DAODSUSY~derivation is processed into n-tuples by analysis specific code.

\section{Monte Carlo}\label{sec:monteCarlo}

This section describes how the benchmark signal is modeled in Monte Carlo simulation. In the benchmark model, stop-antistop events are produced from $pp$-collisions through $R$-parity conserving diagrams. The stop particle is long-lived and decays via an $R$-parity violating coupling. The stop particle has a $100\%$ branching ratio to a muon and $b$-quark, but these signals are interpreted as $\tilde{t}\rightarrow \mu + j$, for reasons discussed below.  

The simulation of signal processes is usually completed in the following steps.
\begin{itemize}
  \item The energy of the colliding particles is estimated using \ac{PDF} sets, discussed in Section~\ref{sec:acceleratorBasics}.
  \item A \textit{generator} models the hard scattering process using Matrix Elements calculations, which input information about the colliding particles and compute the resulting final states.
  \item In \textit{parton showering} step, particles with color-charge probabilistically radiate gluons and quark-antiquark pairs. The resulting particles form final state hadrons.
  \item Additional collisions are overlaid on the event to simulate the effect of pile-up.
  \item Final state particles are propagated through a simulated version of the \ac{ATLAS} detector, modeling interactions with material.
  \item In the \textit{digitization} step, the energy deposited in the detector is converted to digital signals.
  \item Event reconstruction is run on those predicted detector signals, in the same way reconstruction is run on data. 
\end{itemize}

For most signal models, these steps are sufficient. However for the benchmark model considered by this analysis, the process of simulating events is more complicated. Because the stop has color charge, and its lifetime is greater than the timescale for \ac{QCD} hadronization, the stop particle hadronizes with Standard Model particles, forming an $R$-hadron. 

The resulting $R$-hadron can interact with the detector before the $R$-hadron decays, which means several of the above steps need to be repeated. The propagation of the $R$-hadron through the detector needs to be handled before the R-hadron decays. The R-hadron decay then needs to be modeled, and the resulting particles from the decay also need to propagate through the detector. The details of simulating these signals are described below. 

As described in Section~\ref{sec:acceleratorBasics}, $pp$-collisions at $\sqrt{s}=13$~TeV actually involve collisions between the constituents of the proton, not the protons themselves. The fraction of the proton's momentum which is carried by the constituents are described by \ac{PDF} sets. \ac{PDF} sets are informed from experimental measurements, and there is some variation depending on which \ac{PDF} set is used. The signal samples considered here use the NNPDF2.3LO \ac{PDF} set~\cite{Ball:2012cx}, which include measurements from \ac{LHC} data collected by \ac{ATLAS} and \ac{CMS} in 2010 and 2011. 

%though the variations on both the signal acceptance and the production cross section due to the choice of PDF modeling are considered after the fact using the eigenvector prescription of PDF variations. (The usual procedure, but we don't do this)

The next step is to model the hard scattering process resulting from the interaction of two of these proton constituents using a Monte Carlo generator. Generators use Matrix Elements to compute the cross section of a particular process, and also describe the probability to go from
an initial state to a final state with particular kinematic properties. The generator takes each collision and produces a final state. For this analysis, \MGMCatNLOV{2.6.1}~\cite{Alwall:2014hca} is used to produce $\pp \rightarrow \tilde{t}\tilde{\bar{t}}$ events with up to two additional partons from initial or final state radiation, at leading order accuracy. 

The resulting particles from the hard scatter process must undergo hadronization in the parton showering step. This step also includes accounting for the initial and final state radiation of additional partons. Unlike the generator step, the parton showering step is not described by perturbative \ac{QCD}, but by approximations which aim to model the probability of parton splitting or gluon emission. 

It is important to note that both the Matrix Element calculation in the generator step, and the parton shower process can produce events with an additional jet in the final state. In order to avoid double counting of events, a \textit{merging} procedure is performed. This merging procedure ensures that the two leading jets are described by the Matrix Element calculation, while any additional jets may be described by the parton shower step. In the merging procedure, jet reconstruction is performed on events with an anti-$k_\textrm{T}$ algorithm. The resulting jets are classified into high-$\pT$ and low-$\pT$. If all high momentum jets in an event can be matched to a parton produced in the generator step, the event is kept. If not, the event is thrown out. Extra jets are only allowed if their $\pT$ is below threshold, or if they are not the two leading jets in the event. The momentum threshold is usually $\sim 10$~GeV~\cite{Alwal:2014}.

The parton showering step for the signal samples considered here is performed with \PYTHIAV{8.230}~\cite{Sjostrand:2007gs}, which uses the A14 (\ac{ATLAS} 2014) set of tuned parameters for the modeling of the parton shower, hadronization, and underlying event~\cite{ATL-PHYS-PUB-2014-021}. At this point, the $\tilde{t}$ is treated as a stable particle, and is not allowed to decay. \pythia hadronizes the $\tilde{t}$ with Standard Model particles, resulting in a stop $R$-hadron. 

The $\tilde{t}$ may form a bound state with either one or two spectator quarks. About $90\%$ of stop particles form meson-like states ($\tilde{t}\bar{q}$), and $10\%$ form baryon-like states ($\tilde{t}qq$). The mass spectrum of these $R$-hadrons is described by the \textit{generic model}~\cite{Fairbairn:2006gg}. Figure~\ref{fig:rhadron_properties}~\footnote{Note that in this thesis, [au] indicates that the one dimensional distribution has been normalized to unity. The first and last bins of one dimensional histograms also contain underflow and overflow entires unless otherwise specified} shows the mass that the spectator partons contribute to the $R$-hadron, defined as the difference between the $R$-hadron mass and the $\tilde{t}$ mass. Figure~\ref{fig:rhadron_properties} also shows the charge distributions of $R$-hadrons. Roughly $55\%$ of the $R$-hadrons formed have nonzero electric charge, and the two $R$-hadrons cannot have same-sign electric charge.

%JOs for rhadrons
%https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Simulation/SimulationJobOptions/share/specialConfig/preInclude.RHadrons.py?v=21.0
\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/RhadronProperties/hist_massspectator.pdf}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/RhadronProperties/hist_charge.pdf}
    \caption[$R$-hadron signal distributions]{Properties of $R$-hadrons in Monte Carlo. Left: The difference between the $\tilde{t}$ mass and the $R$-hadron mass, or mass contributed to the $R$-hadron by spectator partons. Right: The charge distributions of stable $R$-hadrons.}
    \label{fig:rhadron_properties}
  \end{center}
\end{figure}

At this stage, the events consist of final state particles from the hard interaction and showering, but does not contain particles produced in pile-up interactions, as well as effects due to activity from bunch crossings before or after the selected event. In order to model the effects of in-time and out-of time pile-up, minimum bias events are overlaid on simulated events. Minimum bias events are specifically generated for this process. These multiple interactions are simulated with soft \ac{QCD} processes in \PYTHIAV{8.2186}, which uses the A2~\cite{ATL-PHYS-PUB-2012-003} set of tuned parameters and the MSTW2008LO \ac{PDF} set~\cite{Martin:2009iq}.

Events are then sent to \GEANT (4 v10.1.3) to simulate particles' interactions with the \ac{ATLAS} detector~\cite{Agostinelli:2002hh}. \GEANT contains a simulated version of \ac{ATLAS} with information about the detector materials and their location, as well as the magnetic field. This geometry is periodically updated when upgrades to the \ac{ATLAS} detector are made. Updates are also made in order to account for improvements in understanding the detector material position and chamber alignment. 

Particles' trajectories in the magnetic field and detector are modeled using small steps. At each step a particle's possible interactions are considered. Possible interactions with material include energy loss and the production of new particles. These steps continue until the particle loses all of its energy, or the particle exits the detector volume. \GEANT includes effects such as photon conversions, which can happen in layers of the Inner Detector. 

\GEANT also propagates $R$-hadrons through the detector using a \textit{generic} interaction model~\cite{Kraan:2004tz,Mackeprang:2006gx,Mackeprang:2009ad}. In the generic interaction model model, $R$-hadrons have an interaction cross section of $12$~mb, per spectator parton, per nucleon in the detector. The $\tilde{t}$ is assumed to have no contribution to the $R$-hadron's interaction with the detector. In this interaction model the content of the light-quark system in the $R$-hadron can be altered as it traverses the detector. If the light-quark content of the $R$-hadron changes during an interaction, it is possible that the total electric charge of the $R$-hadron also changes.

The $R$-hadron's lifetime is equivalent to the lifetime of the $\tilde{t}$. When the $R$-hadron decays, it is passed from \GEANT to a second instance of \pythia. \pythia simulates the $\tilde{t}$ decay to a muon and a $b$-quark with branching ratio Br$(\tilde{t})=100\%$ using a parton shower model, also hadronzing the $\tilde{t}$ decay products. The stable daughter particles of the $R$-hadron decay are handed back to \GEANT in order to simulate their interaction with the detector.

It is important to note that the $\tilde{t}$ decay and hadronization of the $\tilde{t}$ decay products, which are performed in \pythia, occur at a single step in \GEANT. Only stable particles from the $\tilde{t}$ decay are handed back to \GEANT. Any information about intermediate particles between the $b$-quark production and its final state decay products is lost. In simulated events, all stable particles from the stop decay are produced at the same location in the detector. As a result, effects of the $b$-quark lifetime, such as tertiary vertices, are not simulated. Due to the lack of tertiary vertices from the $b$-quark decay, these signal samples are interpreted in a $\tilde{t}\rightarrow\mu+q$ decay model, where $q$ is a light-quark.

Once all of the event's particles have propagated through the \ac{ATLAS} detector, the energy deposited into active sensors is converted into signals that resemble the actual detector response in the digitization step. These responses are usually taken from parametrized functions in \ac{ATLAS} specific software, and include effects for electronic noise and dead sensors. 

Once the simulated particles have been converted into detector signals, the same reconstruction software used on data can be used on
the \ac{MC}. Detector signals are reconstructed into particles using the same Large Radius Tracking and secondary vertex reconstruction methods described in Chapter~\ref{chp:reconstruction}. One key difference between events in data and \ac{MC} is that the \ac{MC} keeps original information about the generated particles. This information is referred to as \textit{truth} information, and is often used to measure the efficiency of reconstructing individual particles or selecting signal events. 

\ac{MC} signal samples are prepared for $\tilde{t}$ masses varying from $1$ TeV to $2$ TeV, in steps of $100$~GeV. Samples with lifetimes, $\tau = 0.01$, $0.1$, and $1$ ns are prepared for each $\tilde{t}$ mass sampled. Table~\ref{tab:stop_particle_xs} shows the production cross sections for $pp\rightarrow \tilde{t}\tilde{\bar{t}}$ at $\sqrt{s}=13$ TeV. 

Signal cross sections are calculated to approximate next-to-next-to-leading order in the strong coupling constant, adding the re-summation of soft gluon emission at next-to-next-to-leading-logarithmic accuracy (approximate NNLO+NNLL)~\cite{Beenakker:1997ut,Beenakker:2010nq,Beenakker:2016gmf}. The nominal cross section and the uncertainty are derived using the PDF4LHC15\_mc PDF set, following the recommendations of Ref.~\cite{Butterworth:2015oua}.

\begin{table}[!ht]
\begin{center}
\caption[Stop-antistop production cross sections]{Stop-antistop production (NNLO+NNLL) cross sections in pp-collisions at $\sqrt{s} = 13$ TeV}\label{tab:stop_particle_xs}
 \begin{tabular}{cl}

% NNLO+NNLL
\hline\hline
Stop Mass [GeV] & Cross Section [pb]\\
\hline
1000 & $0.683\cdot10^{-2} \pm 11.2 \%$  \\
1100 & $0.335\cdot10^{-2} \pm 12.12 \%$  \\
1200 & $0.170\cdot10^{-2} \pm 13.13 \%$  \\
1300 & $0.887\cdot10^{-3} \pm 14.21 \%$  \\
1400 & $0.473\cdot10^{-3} \pm 15.37 \%$  \\
1500 & $0.257\cdot10^{-3} \pm 16.63 \%$  \\
1600 & $0.142\cdot10^{-3} \pm 17.96 \%$  \\
1700 & $0.796\cdot10^{-4} \pm 19.4 \%$  \\
1800 & $0.451\cdot10^{-4} \pm 20.94 \%$  \\
1900 & $0.258\cdot10^{-4} \pm 22.6 \%$  \\
2000 & $0.148\cdot10^{-4} \pm 24.38 \%$  \\
\hline\hline

 \end{tabular}
\end{center}
\end{table}

\subsection{Corrections applied to simulation}

The simulation may not perfectly describe data for several reasons. The material description of the detector may not be perfect, and there may be data quality issues which occur during data taking that are not modeled in \ac{MC}. There may also be differences in the pile-up modeling in \ac{MC}, and the distributions observed in data. In order to account for these differences, several corrections to Monte Carlo signal samples, and uncertainties on the observed efficiencies, are applied. These corrections and uncertainties are discussed in more detail in Chapter~\ref{chp:signalSystematics}. Additional Monte Carlo and data samples which are used to evaluate these corrections and uncertainties are the focus of this section.

The efficiency for reconstructing and selecting muons is of particular importance for this analysis. In order to account any differences in efficiency observed in data and \ac{MC}, $Z\rightarrow \mu^{+}\mu^{-}$ events are compared in data and \ac{MC}. In \ac{MC} these events are generated using \POWHEGV{v1 r2856}~\cite{Nason:2004rx} and \PYTHIAV{8.186}~\cite{Sjostrand:2014zea}. These events are reconstructed with standard \ac{ATLAS} software. Additional uncertainties are also applied to account for differences in tracking efficiency, as discussed in Chapter~\ref{chp:signalSystematics}.

The pile-up profile in \ac{MC} may not match the pile-up profile observed in data. The choice of pile-up profile in \ac{MC} is made before data are collected, and is based on expected \ac{LHC} operating parameters. These parameters are often different than what was expected for a variety of reasons, including \ac{LHC} performance, and \ac{ATLAS} luminosity leveling. Separate \ac{MC} \textit{campaigns} try to anticipate the \ac{ATLAS} geometry and \ac{LHC} performance for each year of data taking. Later, analyzers reweight the pile-up profile of the \ac{MC} to match the pile-up profile observed in data. 

The simulated signal samples used in this analysis consist of three separate \ac{MC} \textit{campaigns}, referred to as MC16a, MC16d, and MC16e. MC16a is reweighted to match the pile-up profile of the 2016 dataset, while MC16d is reweighted to 2017, and MC16e is reweighted to 2018. This reweighting procedure is performed in two dimensions, in order to ensure the distributions of the number of primary vertices, as well as the mean number of interactions per bunch crossing match in data and \ac{MC}. 

%The MC16d sample for a stop with mass of $1.8$ TeV and lifetime, $\tau=0.01$ ns has an incomplete pile-up distribution. 

\subsection{$R$-hadron kinematics}\label{sec:rhadKinematics}

This analysis only considers the decay products of the $R$-hadrons. The charge of the $R$-hadron, and the $R$-hadron's potential interaction with the detector should not have an impact on the analysis. Kinematic properties of the $R$-hadron samples are shown below, for different $\tilde{t}$ masses and lifetimes. For these studies, $R$-hadrons are required to decay within $R_{xy}<300$ mm and $|Z|<300$ mm, where a displaced vertex could be reconstructed. Distributions shown in this section, and subsequent sections of this chapter are normalized to unity, to highlight shape differences in distributions rather than overall scale differences due to the stop particle mass. %The highest bin also contains overflow entries, and the lowest bin contains underflow entires. %As described above, the $R$-hadron interactions are simulated in \GEANT and nuclear interactions with detector material can cause changes in the species of $R$-hadron, and therefore the electric charge. This search is only sensitive to the $R$-hadron decay products, and should be insensitive to the details of the interaction model.

Kinematic properties of the $R$-hadron samples are shown below, for different $\tilde{t}$ masses and lifetimes. For these studies, $R$-hadrons are required to decay within $R_{xy}<300$ mm and $|z|<300$ mm. Distributions shown in this section, and subsequent sections of this chapter are normalized to unity, to highlight shape differences in distributions rather than overall scale differences due to the stop particle mass. The highest bin also contains overflow entries, and the lowest bin contains underflow entires. 

In Figure~\ref{fig:rhadron_truth_rxy_z}, the transverse and longitudinal $R$-hadron decay position is shown for three lifetimes. As expected, the decay position in $R_{xy}$ follows an exponentially falling distribution. The $z$ position of the $R$-hadron decay has some spread due to the size of the beam spot, which is roughly $\pm 100$~mm in the $z$-direction. On average, $R$-hadron decays occur further away from the interaction point in $R_{xy}$ and $z$ for $\tilde{t}$ with longer lifetimes.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_lifetime_rxy.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_lifetime_z.pdf}
    \caption[$R$-hadron signal distributions: decay position in $R_{xy}$ and $z$]{Left: $R$-hadron decay position in $R_{xy}$ for different lifetimes. Right: $R$-hadron decay position in $z$ for different lifetimes.}
    \label{fig:rhadron_truth_rxy_z}
  \end{center}
\end{figure}

In Figure~\ref{fig:rhadron_betagamma_momentum}, the $R$-hadron $\beta\gamma$ and transverse momentum is shown for three different $\tilde{t}$ masses. $R$-hadrons with lower masses tend to be more boosted, and have larger $\beta\gamma$. $R$-hadrons with larger mass tend to have larger transverse momentum. Due to the nature of pair-production of heavy particles at a hadron collider, particles produced with a larger pseudorapidity tend to be more boosted and have a larger value of $\beta\gamma$.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_betagamma.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_pt.pdf}
    \caption[$R$-hadron signal distributions: $\beta\gamma$ and $\pT$]{Left: $R$-hadron $\beta\gamma$ for different stop masses. Right: $R$-hadron transverse momentum for different stop masses.}
    \label{fig:rhadron_betagamma_momentum}
  \end{center}
\end{figure}

Figure~\ref{fig:rhadron_eta_phi}, shows the $R$-hadron $\eta$ and $\phi$ for three different $\tilde{t}$ masses. There is no significant change in the $\phi$ distributions for different $\tilde{t}$ masses. Distributions of $R$-hadrons are symmetric in $\phi$ as expected. Due to the kinematics of pair-produced heavy particles at a hadron collider, slight differences in the $\eta$ distributions are observed for $\tilde{t}$ particles with different masses. The width of the $\eta$ distribution decreases with increasing $\tilde{t}$ mass.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_eta.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_phi.pdf}
    \caption[$R$-hadron signal distributions: $\eta$ and $\phi$]{Left: $R$-hadron $\eta$ for different $\tilde{t}$ masses. Right: $R$-hadron $\phi$ for different $\tilde{t}$ masses.}
    \label{fig:rhadron_eta_phi}
  \end{center}
\end{figure}


Figure~\ref{fig:rhadron_ntrack_mtrack}, shows the number of stable daughter particles from the $R$-hadron decay which have electric charge~$= \pm 1$ and $\pT >1$ GeV. The selection applied to the $R$-hadron decay products is intended to study the number of daughter particles which could be reconstructed as Inner Detector tracks. Stop quarks with larger mass tend to produce more reconstructible decay products. The mass which would be reconstructed from these decay products is also shown. Stop quarks with larger mass tend to have larger reconstructible mass, and the reconstructible mass is on average two thirds of the stop particle mass. Roughly one third of the stop particle decay products are electromagnetically neutral or have transverse momentum less than $1$~GeV.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_nCh1GeV.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_mCh1GeV.pdf}
    \caption[$R$-hadron signal distributions: number and mass of charged particles]{Left: Number of charge $=\pm1$ particles with $\pT > 1$ GeV produced in the $R$-hadron decay, referred to as the $R$-hadron's number of tracks. Shown for different $R$-hadron masses.  Right: Invariant mass of the $R$-hadron daughter particles with charge $= \pm 1$ and $\pT > 1$ GeV, referred to as $R$-hadron's track mass. This includes the momentum contribution from the muon in the decay of the $\tilde{t}$. Shown for different $R$-hadron masses.}
    \label{fig:rhadron_ntrack_mtrack}
  \end{center}
\end{figure}

\subsection{Muon kinematics}

This section discusses kinematics of the muons produced in $R$-hadron decays, and how these kinematics depend on the stop mass and lifetime. Muons are required to be produced from $R$-hadrons which decay with $R_{xy}<300$~mm and $|z|<300$~mm. 

Figure~\ref{fig:truthmuon_d0_pt} shows the transverse impact parameter of muons produced from stop particles with different lifetimes. Stop quarks with larger lifetimes result in muons with larger impact parameters. Stop particles with $\tau(\tilde{t})=0.01$~ns, result in muons with impact parameters less than $20$~mm, while stop particles with $\tau(\tilde{t})=0.1$~ns result in muons with transverse impact parameters less than $60$~mm. Stop particles with $\tau(\tilde{t})=1$~ns result in muons with impact parameters as large as $300$~mm. Figure~\ref{fig:truthmuon_d0_pt} also shows the muon transverse momentum for different $\tilde{t}$ masses. Stop quarks with larger masses result in higher $\pT$ muons.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_lifetime_muon_d0.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_muon_pt.pdf}
    \caption[Properties of muons produced in $R$-hadron signals: $|d_0|$ and $\pT$]{Properties of muons produced in the $R$-hadron decay, referred to as truth muons. Left: Truth muon transverse impact parameter, $|d_0|$, for different $\tilde{t}$ lifetimes. Right: Truth muon transverse momentum, for different $\tilde{t}$ masses.}
    \label{fig:truthmuon_d0_pt}
  \end{center}
\end{figure}

Figure~\ref{fig:truthmuon_eta_phi} shows the muon $\eta$ and $\phi$ distributions for different $\tilde{t}$ masses. The width of the muon $\eta$ distribution slightly decreases with increasing stop particle mass. The muons have a flat $\phi$ distribution, which does not greatly change as a function of $\tilde{t}$ mass.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_muon_eta.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_muon_phi.pdf}
    \caption[Properties of muons produced in $R$-hadron signals: $\eta$ and $\phi$]{Properties of muons produced in the $R$-hadron decay, referred to as truth muons. Left: Truth muon $\eta$ for different $\tilde{t}$ masses. Right: Truth muon $\phi$ for different $\tilde{t}$ masses.}
    \label{fig:truthmuon_eta_phi}
  \end{center}
\end{figure}

\subsection{Track kinematics}

$R$-hadron daughter particles with electric charge $= \pm 1$, transverse momentum $\pT>1$ GeV, which are not muons, are referred to as $R$-hadron truth tracks. Properties of these $R$-hadron truth tracks are shown in this section. This selection is designed to study properties of reconstructible particles from the hadronic jet produced in the $\tilde{t}$ decay. 

Figure~\ref{fig:rhadron_track_d0_pt} shows the $R$-hadron track $|d_0|$ for different $\tilde{t}$ lifetimes. Stop quarks with longer lifetimes result in daughter particles with larger transverse impact parameters. The $R$-hadron track transverse momentum is also shown for different $\tilde{t}$ masses. Larger masses result in tracks with larger $\pT$.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_lifetime_track_d0.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_track_pt.pdf}
    \caption[Properties of charged particles produced in $R$-hadron signals: $|d_0|$ and $\pT$]{Properties of charged particles with $\pT>1$~GeV produced in the $R$-hadron decay, referred to as truth tracks. Left: $R$-hadron track transverse impact parameter, $|d_0|$, for different $\tilde{t}$ lifetimes. Right: $R$-hadron track transverse momentum, for different $\tilde{t}$ masses.}
    \label{fig:rhadron_track_d0_pt}
  \end{center}
\end{figure}

Figure~\ref{fig:rhadron_track_eta_phi} shows the $R$-hadron track $\eta$ and $\phi$ distributions for different $\tilde{t}$ masses. These distributions do not greatly change as a function of $\tilde{t}$ mass.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_lifetime_track_eta.pdf}
    \includegraphics[width=0.45\textwidth]{plots/TruthDistributions/Compare_mass_track_phi.pdf}
    \caption[Properties of charged particles produced in $R$-hadron signals: $\eta$ and $\phi$]{Properties of charged hadrons with $\pT>1$~GeV produced in the $R$-hadron decay, referred to as truth tracks. Left: $R$-hadron track transverse impact parameter, $\eta$, for different $\tilde{t}$ lifetimes Right: $R$-hadron track $\phi$, for different $\tilde{t}$ masses.}
    \label{fig:rhadron_track_eta_phi}
  \end{center}
\end{figure}



