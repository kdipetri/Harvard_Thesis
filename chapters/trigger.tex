\chapter{Trigger}
\label{chp:trigger}

%{\color{red} First draft mostly done except muon spectrometer only section and a few figures}

%{\color{red} Done melissa's comments}

Due to limitations in data storage, transfer rates, and computing power, only a small fraction of the collisions produced at the \ac{LHC} can be recorded. Deciding which events should be saved for future analysis and which should be thrown away is the job of the trigger. The importance of the trigger cannot be overstated. If the trigger does not select an interesting event or physics process, that event is lost forever. A good trigger must balance the selection efficiency for a process of interest with strong background rejection to reduce the rate. Selections must be robust, and based on well-understood features that can separate interesting physics processes from backgrounds. 

\ac{ATLAS} uses a multi-stage approach to quickly decide which events are interesting. The first stage must process all of the data quickly, and is referred to as the \ac{L1} trigger. The \ac{L1} trigger is hardware-based, regional, and uses coarse detector information. The \ac{L1} trigger must make a decision in $2.5~\microsec$, and reduces the event rate from $40$~MHz to $100$~kHz. The second stage is referred to as the \ac{HLT}. Because the \ac{HLT} processes fewer events than \ac{L1}, it has more time to make a decision, on average $200$~ms, and can make use of higher granularity information. The \ac{HLT} is software-based, and reduces the event rate to $1-1.5$ kHz. 

Finding a trigger which can select events with displaced signatures is a challenge for nearly every long-lived particle analysis. In order to keep the rate of selected events manageable, the \ac{ATLAS} trigger usually assumes all particles are produced at the $pp$-interaction point. For example, triggers that select events with electrons or muons typically require the leptons have impact parameters consistent with the $pp$-interaction point ($|d_0|<10$~mm).

There are a few strategies which \ac{ATLAS} long-lived particle searches can use to trigger on events:
\begin{enumerate}
 \item Trigger on prompt objects in the event using standard triggers. The effectiveness of this strategy is model dependent, and in some cases can hurt sensitivity by one or two orders of magnitude. 
 \item Trigger on displaced jets or real $\MET$ using standard jet or $\MET$ triggers. The \ac{ATLAS} jet and $\MET$ triggers do not explicitly associate tracks to jets or Calorimeter activity. These triggers may be used for long-lived particle decays which occur inside or before the Calorimeter detector volume. In the case of the $\MET$ trigger, it may also be used to trigger on long-lived particles which escape the Calorimeter volume. This strategy is most effective for signal models with heavy particles which produce jets with large transverse momentum or events with large $\MET$, and is generally less effective for probing lighter long-lived particles.
 \item Use a trigger in a way it was not intended. For example, a photon trigger can be used to trigger on events with displaced electrons. This strategy extends sensitivity from $|d_0|<10$~mm to several centimeters, at the cost of increasing the momentum threshold (in 2018, this strategy would increase the $\pT$ threshold from $\pT>26$ to $140$ GeV). The $\MET$ trigger can also be used to trigger on displaced muons, again trading in sensitivity to larger displacements for higher $\pT$ thresholds. 
 \item Design a trigger specific to a long-lived particle signature. This process is work-intensive, and must overcome the challenge of finding a suitable hardware-based trigger. Usually, these specialized triggers are only allocated a limited amount of bandwidth, and cannot exceed a rate of $\sim10$ Hz. 
\end{enumerate} 

This analysis uses two triggers to select events. The first trigger is specifically designed to select events with a displaced muon. The second trigger selects events with significant $\MET$, as measured by the Calorimeter. Even though the benchmark signal model considered by this analysis does not produce particles which are invisible to the \ac{ATLAS} detector, the high momentum muons only deposit a fraction of their energy in the Calorimeter, resulting is large apparent $\MET$, and a $\MET$ trigger. 

The \ac{ATLAS} trigger scheme is described in Section~\ref{sec:ATLAS_trigger_scheme}. The Muon Spectrometer trigger is described in Section~\ref{sec:muonTrig}, with an emphasis on the specific trigger used in this analysis. The $\MET$ trigger is described in Section~\ref{sec:missingEtTrig}. A paper which uses data collected in 2015 provides more detail about the \ac{ATLAS} trigger scheme and performance in $\sqrt{s}=13$~TeV~\cite{TRIG-2016-01}, including triggers not discussed in this thesis.

\section{Trigger scheme}\label{sec:ATLAS_trigger_scheme}

The \ac{LHC} runs with a bunch spacing of $25$ ns, corresponding to a peak \textit{event rate}, or bunch crossing rate, of $40$ MHz. Each \ac{ATLAS} event contains $\sim 25$~MB of \texttt{RAW}, detector-level data. When reconstructed, the event size can be reduced to $\sim 1$~MB of data. If \ac{ATLAS} were to save every reconstructed \ac{LHC} collision, this would result in an accumulation of $40$ TB of data per second, and nearly 150 petabytes per hour. It is impossible to store and process this much data, and the dataset would consist of mostly unremarkable events. 

Given the size of a full \ac{ATLAS} event, and the rate at which data can be transferred, it is only possible to save data at a rate of $\sim 1$ kHz. This means that \ac{ATLAS} on average can only keep one event out of every 40,000. The \ac{ATLAS} trigger is designed to quickly select which events are saved for future analysis, reducing the dataset to a manageable size using a multi-step triggering scheme. The first stage is referred to as \ac{L1} and the second as \ac{HLT}. The \ac{ATLAS} trigger scheme is summarized in Figure~\ref{fig:ATLAS_trigger}, and discussed in more detail below.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{plots/Trigger/ATLASTrigger.png}
    \caption[Schematic of the ATLAS trigger and data acquisition system]{Schematic of the \ac{ATLAS} trigger and data acquisition system~\cite{Ruiz-Martinez:2133909}}
    \label{fig:ATLAS_trigger}
  \end{center}
\end{figure}

The \ac{ATLAS} \textit{trigger menu} consists of the lists of triggers used to select which events are saved for future analysis. The menu consists of different \ac{L1} \textit{trigger items}, \ac{HLT} \textit{trigger chains}, and their \textit{trigger prescales}. A single \ac{L1} item can seed many \ac{HLT} trigger chains, but each \ac{HLT} trigger is usually seeded by a single \ac{L1} item. A typical \ac{L1} item is \verb=L1_MU20=, which is for a single muon with a momentum threshold of $20$~GeV. A typical \ac{HLT} chain is \verb=HLT_mu26_ivarmedium=, which selects events with a muon that has $\pT>26$~GeV, passes an isolation requirement, and is seeded by \verb=L1_MU20=. Prescales determine the rate at which a specific trigger item or chain should be accepted. For example, a prescale of $100$ means that $1$ out of $100$ events that fired the trigger are accepted. 

At \ac{L1}, detectors with fast readout are used in a hardware-based trigger, which reduces the event rate from $40$~Mhz to $100$~kHz. This output rate is limited by how quickly the detector can be read out as well as how much data the \ac{HLT} can process. The \ac{L1} trigger uses custom \ac{ASIC}s and \ac{FPGA}s located near the detectors, and must make a decision in less than $2.5~\mu\textrm{s}$. This includes up to $70$~ns for a particle's time of flight from the interaction point to the detector (for the Muon Spectrometer end-caps), and roughly $1~\mu\textrm{s}$ needed to transmit signals between on-detector electronics and dedicated trigger electronics in a neighboring counting room. 

The \ac{L1} trigger uses coarse detector information from the Muon Spectrometer and Calorimeter. The \ac{L1Muon} looks for coincidences in different layers of the \ac{RPC} and \ac{TGC} that are consistent with muons produced from $pp$-interactions. The Calorimeter information used for the \ac{L1} trigger is referred to as \ac{L1Calo}. \ac{L1Calo} uses coarse grained information from all Calorimeter systems, and is responsible for all other triggers besides those used for muons. This includes triggers for electrons, photons, jets, taus, event transverse energy, and $\MET$. No information from the Inner Detector is used at \ac{L1} because of the large number of channels and long time it would take to reconstruct tracks. 

About one third of events which fire a \ac{L1} trigger are selected by the Muon trigger, another third by the Electromagnetic Calorimeter trigger, and the remaining third by Hadronic Calorimeter triggers, or combined objects like a muon trigger plus an \ac{ECAL} deposit. At \ac{L1}, the Calorimeter and Muon triggers can flag the same event. 

Figure~\ref{fig:ATLAS_L1_trigger_rates} shows the trigger rate for several \ac{L1} items during a run taken in 2018. This run had peak luminosity of $2.0 \times 10^{34} \textrm{cm}^{-2}\textrm{s}^{-1}$ and a peak average number of interactions per crossing of $\langle \mu \rangle=56$. A few representative single-object triggers are shown, which have not been prescaled. All trigger rates decay with decreasing instantaneous luminosity. Rates also increase periodically due to \ac{LHC} luminosity re-optimizations. There are also dips in rate due to dead-time, as well as spikes caused by detector noise.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Trigger/TrigOpsPublicWinter2019_L1_single-item_rates_ATLASStyle_359872}
    \caption[Level 1 Trigger rates]{\ac{L1} Trigger rates during a run from 2018. Trigger items are based on electromagnetic clusters (EM), muon candidates (MU), jet candidates (J), missing transverse energy (XE) and tau candidates (TAU). The number in the trigger name denotes the trigger threshold in GeV. The letters following the threshold values refer to details of the selection, including variable thresholds (V), hadronic isolation (H), and electromagnetic isolation (I)~\cite{ATLASTrigger}.}
    \label{fig:ATLAS_L1_trigger_rates}
  \end{center}
\end{figure}

If an event passes a \ac{L1} trigger, that information is sent to the \ac{CTP}. This information includes any \ac{RoI} identified by the \ac{L1Muon} and \ac{L1Calo}. The \ac{CTP} also applies any prescales. The \ac{CTP} distributes the signal that an event has been accepted at \ac{L1} to the full detector via optical cables to on-detector buffers (which hold $2.5~\mu\textrm{s}$ of data). When the front-end electronics receive a \ac{L1} signal the data are sent to off-detector read-out drivers in the counting room, where they are packaged and processed. Read-out drivers are detector specific, and re-package the data before sending it in a common \ac{ATLAS} format to the read-out servers. Read-out drivers also have sufficient processing power to monitor the data and even perform basic calibrations. Data are then sent to read-out servers, where they await a decision from the \ac{HLT}. Unlike read-out drivers, read-out servers are common to all sub-detectors. 

The \ac{HLT} is a software based trigger, which uses a computing farm of more than 20,000 processors to reduce the event rate to $1-1.5$ kHz. Any prescales applied to \ac{HLT} triggers are applied before the decision is made, in order to save computing time. Many \ac{HLT} algorithms use a two step process in order to reduce the time it takes to make a decision. First a rough pass at reconstruction is made inside any \ac{RoI} identified by the \ac{L1} trigger. In the second stage, more complex reconstruction techniques, which are very similar to the offline reconstruction methods described in Chapter~\ref{chp:reconstruction} and utilize the full granularity of the detector are used. The \ac{HLT} is the first stage in which \ac{ID} information is incorporated into the trigger. 

The \ac{HLT} has on average $200$~ms to make a decision. It is impossible to reconstruct all of an event's \ac{ID} tracks in the alloted time. In order to overcome the strict processing time constraints at \ac{HLT}, Inner Detector tracks are only reconstructed inside regions of interest identified by \ac{L1}. In order to reduce CPU usage, \ac{ID} track reconstruction is performed in two steps. In the first step, trigger specific pattern recognition is used to identify track seeds. In the second step, those seeds are used to reconstruct precision tracks, with methods very similar to those used in standard offline reconstruction. About $40\%$, $35\%$ and $15\%$ of the processing time at \ac{HLT} is spent on Inner Detector tracking, Muon Spectrometer reconstruction, and Calorimeter reconstruction, respectively.

About $30\%$ of events selected by the \ac{HLT} are used for electron and muon triggers. About $20\%$ of events are selected by hadronic triggers, and $15\%$ by $\MET$ triggers. The remaining available bandwidth is divided equally among photon, tau lepton and $B$-physics triggers. Figure~\ref{fig:ATLAS_HLT_trigger_rates} shows \ac{HLT} trigger rates during a run from 2018 (the same run as Figure~\ref{fig:ATLAS_L1_trigger_rates}). Like the \ac{L1} rates, \ac{HLT} trigger rates decay with decreasing instantaneous luminosity, and exhibit similar dips and spikes due to dead-time and detector noise. 
 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Trigger/TrigOpsPublicWinter2019_HLT_group_rates_ATLASStyle_359872}
    \caption[High Level Trigger rates]{\ac{HLT} Trigger rates during a run from 2018. Individual groups of triggers specific to trigger physics objects are presented. Each group contains single-object and multi-object triggers. Overlap between different groups is only accounted for in the total main physics stream rate. The combined group represents multiple triggers of different objects, as combinations of electrons, muons, taus, jets and missing transverse energy. B-physics and Light States (LS) triggers which are based on muon objects are not included here~\cite{ATLASTrigger}.}
    \label{fig:ATLAS_HLT_trigger_rates}
  \end{center}
\end{figure}

Events selected by the \ac{HLT} are saved in \texttt{RAW} format at the \ac{CERN} Tier-$0$ facilities as separate data \textit{streams}. Events used for physics analysis are saved as \verb=physics_Main=. A subset of \verb=physics_Main= (approximately $10$ to $20$ Hz) is written to an express stream, which is immediately reconstructed in order to begin important steps for calibration and data quality checks before reconstructing the full \verb=physics_Main= dataset. There are also additional streams for calibration, monitoring, and detector performance studies. Tier-0 processes approximately one petabyte of data every day.

Events which cannot properly be processed by the \ac{HLT} are saved to a \textit{debug} stream. The most common reason events are sent to the debug stream is due to long processing times (several minutes per event). Events with many muon tracks have the longest processing times, especially those due to jets which are not fully contained in the Calorimeter. 

\section{Muon Spectrometer Trigger}\label{sec:muonTrig}

Many physics processes of interest at the \ac{LHC} result in a final state muon. These processes include several decay modes of the Standard Model Higgs boson, the $W$ and $Z$ vector bosons, and many hypothetical new particles. Final state muons are expected to be produced with a wide range of momenta, necessitating a high performing muon trigger. The \ac{ATLAS} muon trigger is designed to work with high efficiency and low transverse momentum thresholds in the presence of challenging backgrounds. 

The \ac{L1} muon trigger works to match \ac{RPC} or \ac{TGC} hits to pre-defined roads. These hits must be coincident in space and time, and consistent with a muon produced at the $pp$-interaction point. Several triggers are defined with different momenta threshold, and are separately categorized as low-$\pT$ or high-$\pT$ thresholds. 

The transverse momentum of the muon candidate is estimated by comparing the muon candidate's hit pattern to the expected the hit pattern of a muon with infinite momentum, which would form a straight line. The degree of deviation in hit patterns is treated as the width of a road, and wider roads correspond to the paths of lower momentum muons. Low-$\pT$ triggers also require fewer hit coincidences within a road than high-$\pT$ triggers. High-$\pT$ muons can fire the low-$\pT$ threshold triggers, but low-$\pT$ muons would produce hits outside a narrow road designed for a high-$\pT$ muon. Figure~\ref{fig:muon_trig_roads} shows a schematic of typical muon tracks that would generate low-$\pT$ and high-$\pT$ \ac{L1} triggers in both the barrel and the end-cap.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Trigger/MSTriggerRoads.png}
    \caption[Example muon tracks generating Level 1 triggers in the Muon Spectrometer]{ Typical muon tracks generating triggers in the Muon Spectrometer barrel and end-cap at \ac{L1}~\cite{Aloisio:947275}. }
    \label{fig:muon_trig_roads}
  \end{center}
\end{figure}

In the barrel, there are three layers of \ac{RPC} chambers (RPC1, RPC2, and RPC3 in Figure~\ref{fig:muon_trig_roads}). Hit coincidences are required in two of three layers (RPC1 and RPC2) in order to produce a low-$\pT$ trigger. Hits are required in all three layers to produce a high-$\pT$ trigger. 

In the end-cap \ac{L1} hit requirements are complicated by large backgrounds from low energy protons produced in the end-cap toroid. The toroid sits between the Inner and Middle end-cap stations. Interactions with the end-cap toroid result in these low energy protons, which in turn produce hits in the Middle and Outer station of the end-cap, but not the Inner station. 

In Run~1, the Inner station \ac{TGC} chambers were not included as a \ac{L1} requirement. A large rate of triggers would be produced from these low energy protons. These triggers are not associated with real muons and use up valuable bandwidth. Figure~\ref{fig:fake_muon_triggers} shows how without any information from the Inner station \ac{TGC} chambers, it is very difficult to tell whether or not a \ac{L1} trigger is consistent with a muon produced at the $pp$-interaction point or from interactions in the toroid. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Trigger/fakeTriggers.png}
    \caption[Fake Level 1 triggers produced in the Muon Spectrometer end-cap]{ Fake \ac{L1} muon triggers produced in the Muon Spectrometer end-cap. Only the Inner and Middle stations of the end-cap are shown. Track A leaves a track in all detector regions and points towards the interaction point. Track B gives only a signal in the Big Wheel and Track C does not point towards the interaction point. Only Track A corresponds to an acceptable muon.~\cite{Lösel:2042404}. }
    \label{fig:fake_muon_triggers}
  \end{center}
\end{figure}

As a general rule, \ac{TGC} hits are required in all three layers of the \ac{TGC} which sandwich the Middle station of the \ac{MDT} (TGC1, TGC2, and TGC3 in Figure~\ref{fig:muon_trig_roads}). In some regions, exceptions are made to improve acceptance for very low-$\pT$ muons.  In Run~2, hits in the Inner station chambers (TGC EI and FI) are also required, aiming to reduce the rate of \ac{L1} coincidences from background. Adding this coincidence reduced the overall \ac{L1} muon trigger rate by $\sim 20\%$ with a $1\%$ decrease in efficiency~\cite{L1Muon}. Unfortunately, the Inner station does not have full \ac{TGC} coverage in $\eta$ and $\phi$, and high trigger rates are still a challenge for \ac{ATLAS}. 

In 2016, new \ac{RPC} chambers were included in the trigger. These chambers are located in the feet region, and improved efficiency by $4\%$ in the barrel. Figure~\ref{fig:muon_trig_newRPC} shows the improvement in \ac{L1} trigger efficiency from adding the new chambers. The change in trigger efficiency for \verb=L1_MU11= is shown for $\phi$-sectors 12, and 14, which refer to the feet region. In both $\phi$-sectors, adding the new chambers improves the overall efficiency, and makes the efficiency more constant as a function of $\eta$. With the new chambers, the detector coverage is $\sim80\%$ in the barrel, and $\sim90\%$ in the end-caps. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Trigger/LHCC_Sep2017_sec12_MU11}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Trigger/LHCC_Sep2017_sec14_MU11}
    \caption[Improvement in Level 1 trigger efficiency from new RPC Chambers]{ \ac{L1} trigger efficiency before and after including new \ac{RPC} chambers~\cite{L1Muon}. The efficiency is computed with a subset of 2017 data, with respect to offline muon candidates which are which pass a Medium quality requirement, are isolated, and have a transverse momentum of at least $15$~GeV. The trigger efficiency is shown for (left) $\phi$-sector 12 and (right) $\phi$-sector 14, which refer ot the feet region.  }
    \label{fig:muon_trig_newRPC}
  \end{center}
\end{figure}

\ac{ATLAS} has six \ac{L1} muon trigger items, three for low-$\pT$ muons and three for high-$\pT$ muons. The low-$\pT$ muon items are \verb=L1MU4=, \verb=L1MU6=, and \verb=L1MU10= GeV. In 2015-2016, the three high-$\pT$ muon items were \verb=L1MU11=, \verb=L1MU15=, and \verb=L1MU20=, where \verb=L1MU11= required a $10$~GeV threshold and coincidences consistent with a high-$\pT$ muon. In 2017-2018, the three high-$\pT$ items were \verb=L1MU11=, \verb=L1MU20=, and \verb=L1MU21=. The last two items both applied a $20$~GeV thresholds, but \verb=L1MU21= included the new \ac{RPC} chambers in the feet while \verb=L1MU20= did not.

The \ac{HLT} receives information regarding which trigger item fired at \ac{L1}, including the region of interest and $\pT$ threshold, as well as precision measurements from the \ac{MDT} and \ac{CSC}. The \ac{HLT} muon reconstruction is split into two steps in order to speed up processing time. 

In the first step, the muon candidate from \ac{L1} is refined by including precision hits from the \ac{MDT} and \ac{CSC} which are inside the region of interest. Regions of interest are $(0.1 \times 0.1)$ in the barrel and $(0.03 \times 0.03)$ in the end-cap in $(\Delta \eta \times \Delta \phi$). Precision hits and positions are used to quickly reconstruct a muon track, and the transverse momentum of the muon track is computed from a look up table. %These quickly reconstructed muon tracks are referred to as \textit{fast}~\ac{MS}\textit{-only muons}. %For standard muon triggers, the \ac{MS}-only muon is also extrapolated to the interaction point, and combined with an Inner Detector track to form a \textit{combined muon}. 

The second step of \ac{HLT} muon reconstruction is referred to as the precision stage. This step begins from the muon candidates reconstructed in the first step as seeds. Methods similar to the offline reconstruction (described in Section~\ref{sec:muonReco}) are used to reconstruct segments and tracks from hits in precision and trigger chambers. Precision muon tracks, also known as \ac{MS}-only muons, are first reconstructed in the muon detectors, and are then combined with Inner Detector tracks into combined muons. 

If no matching \ac{ID} track is found, combined muon candidates are searched for by extrapolating \ac{ID} tracks to the \ac{MS}. This inside-out approach is one of the most time intensive algorithms in the \ac{HLT}, and is only used if the outside-in approach fails. Additional requirements, including requiring a muon be isolated from other Inner Detector tracks, may also be applied in order to maintain a reasonable trigger rate at low transverse momentum thresholds.  

The average time for the fast reconstruction step to reconstruct a \ac{MS}-only candidate is $\langle t \rangle=8.2$~ms. The average time to combine this candidate with Inner Detector information is $\langle t \rangle=6.2$~ms. On average, the precision reconstruction step takes $239.5$ ms. However, this step has a long tail which extends out to processing times on the order of seconds. 

Figure~\ref{fig:muon_trigger_eff} shows the muon trigger efficiency in the barrel of \ac{MS}, measured using a subset of the 2018 data. The trigger efficiency is computed with respect to offline muons which pass an isolation and a \texttt{Medium} quality requirement. The \ac{L1} muon trigger in the barrel detector region has an efficiency of approximately $70\%$. Reductions in trigger efficiency at \ac{L1} are due to lack of \ac{RPC} detector coverage near $\eta \approx 0$ and in the feet of the detector. 
%Only statistical data uncertainties are shown. 
%(isolation computed using inner detector tracks reconstructed online by the HLT within a cone with a variable size which depends on the $\pT$ of the muon.) 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plots/Trigger/LHCC2018_HLT_mu26_ivarmedium_OR_HLT_mu50_Medium_IsoFixedCutTightTrackOnly_barrel_probe_pt_eff.png}
    \includegraphics[width=0.48\textwidth]{plots/Trigger/LHCC2018_HLT_mu26_ivarmedium_OR_HLT_mu50_Medium_IsoFixedCutTightTrackOnly_barrel_probe_phi_barrel_eff.png}
    \caption[Level 1 and High Level Trigger efficiency for muons with $|\eta|<1.05$]{Muon Trigger efficiency at \ac{L1} and \ac{HLT} for muons with $|\eta|<1.05$, measured using $Z\rightarrow \mu\mu$ events in 2018 data. Left: efficiency versus muon $\pT$, Right: efficiency versus muon $\phi$~\cite{MuonTrigger}.}
    \label{fig:muon_trigger_eff}
  \end{center}
\end{figure}

The \verb=HLT_mu26_ivarmedium= trigger is seeded by the \verb=L1_MU20= trigger, and requires that a candidate satisfy a $26$ GeV $\pT$ threshold, and pass a medium isolation requirement. The \verb=HLT_mu50= trigger is also seeded by \verb=L1M_U20= at \ac{L1} and requires that a candidate satisfy a $\pT$ threshold of $50$ GeV, with no isolation or quality requirement. In \ac{ATLAS} it is standard to use both \ac{HLT} muon triggers. Isolation and quality requirements are made for the lower threshold trigger, in order to reduce the rate of muons from hadron decays which fire the trigger. No such requirements need to be applied to higher momentum muons, which are produced at a lower rate. The \ac{HLT} trigger efficiency is nearly $100\%$ when computed with respect to muons which pass a \ac{L1} trigger. 

%The momentum resolution for combined and \ac{MS}-only muon triggers is shown in Figure {\TODO blah}. Good momentum resolution ensures a sharp turn-on in efficiency as a function of muon $\pT$. The momentum resolution is better for combined muons than \ac{MS}-only muons due to the Inner Detector measurement, especially for muons with lower transverse momentum. For \ac{MS}-only muons, the momentum resolution is generally better in the barrel than in the end-cap, due to differences in granularity. In the barrel, the momentum resolution is degraded at low-$\pT$, because these muons bend in a way that they do not traverse the entire Muon Spectrometer. 

%{\TODO muon pt resolution}

Most muon triggers require combined muons, in which the \ac{ID} track is strictly required to have a transverse impact parameter $|d_0|<10$ mm. However, several long-lived particle analyses use of triggers which only require \ac{MS}-only muons. Because these \ac{MS}-only triggers are agnostic to Inner Detector activity, it is possible to trigger on muons produced from long-lived particles. The \ac{MS}-only trigger used in this analysis is described in Section~\ref{sec:msonly_trigger}. 

\subsection{Muon Spectrometer Only Trigger}\label{sec:msonly_trigger}

Muons produced in long-lived particle decays often have displacements which are too large to produce a combined muon at \ac{HLT}. In order to recover efficiency that would be lost from using standard muon triggers, dedicated \ac{MS}-only triggers are used in several long-lived particle analyses. However, because these \ac{MS}-only triggers are agnostic to Inner Detector activity, they suffer from a large background rate, especially in the end-caps. 

In order to overcome these rate limitations, the Run~2 Muon Spectrometer only trigger is limited to the barrel ($|\eta|<1.05$), and has a high momentum threshold which requires $\pT>60$ GeV. This trigger is referred to as \verb=HLT_mu60_0eta105_msonly=. This trigger is fully efficient for muon with $\pT>62$ GeV. Muons with transverse momentum $\pT>62$ GeV and $|\eta|<1.05$ are considered in the trigger acceptance. Figure~\ref{fig:muon_trigger_eff_d0} shows the Muon Spectrometer only trigger efficiency as a function of muon $|d_0|$. This efficiency is measured in simulated samples, by computing the fraction of reconstructed muons in acceptance which fired the trigger.


\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/Trigger/MSOnlyTriggerEffd0.png}
    \caption[Muon Spectrometer only trigger efficiency]{Muon Spectrometer only trigger efficiency as a function of muon transverse impact parameter $|d_0|$.}
    \label{fig:muon_trigger_eff_d0}
  \end{center}
\end{figure}

For prompt muons in acceptance, the trigger has an efficiency of approximately $75\%$, as measured in simulation. However, this efficiency is reduced as a function of increasing muon transverse impact parameter, and is less than $50\%$ for muons with impact parameters $|d_0|>200$~mm. This reduction in efficiency is due to the Level 1 roads, which were optimized for prompt muons that point back to $pp$-interaction point. %Figure~\ref{fig:muon_trigger_eff_d0} shows the Muon Spectrometer only trigger efficiency as a function of muon $|d_0|$. This efficiency is measured in simulated samples, by 

%{\color{red} Include some signal plots to show acceptance and efficiency of our signal in Muon Trigger}. 

%For the $\tilde{t}$ signals considered in this analysis, each event has two muons in the final state, and therefore two chances to fire the muon trigger. However, these two chances do not make up for the reduction in acceptance from the strict pseudorapidity requirements. 

\section{Missing Transverse Energy Trigger}\label{sec:missingEtTrig}

While offline $\MET$ definitions usually include muons in the $\MET$ calculation, the \ac{L1} and \ac{HLT} trigger definitions do not. Only energy measurements in the Calorimeter are used in the trigger. Because muons only deposit a fraction of their energy in the Calorimeter, events with high momentum muons can also fire a $\MET$ trigger.

In \ac{ATLAS}, the $\MET$ trigger is part of the Level 1 Calorimeter trigger. Rather than read out information from all cells individually, \ac{L1Calo} sums up signals from groups of neighboring cells called \textit{trigger towers}. The analog signals from these neighboring cells are added, digitized, calibrated, and read out as a single tower. Trigger towers have granularity $(0.1 \times 0.1)$ in $(\Delta \eta \times \Delta \phi)$~in the central region and $(0.4 \times 0.4)$ in the forward region. There are 7168 towers in total, which are split between the Electromagnetic and Hadronic Calorimeters. 

The \ac{L1Calo} compiles several types of trigger items. Central ($|\eta|<3.2$) trigger towers are used for electrons and photons, taus, and jets. Trigger towers in the Forward Calorimeter ($|\eta|>3.2$) are used for forward jet triggers. All trigger towers, (both central and forward) are used to define global triggers, for variables such as $\MET$ and $\MET$-significance.  

The \ac{L1} $\MET$ is computed using combinations of adjacent trigger towers in $\eta$ and $\phi$, which are referred to as \textit{jet elements}. These jet elements consist of $2$ adjacent towers in $\eta$ and $2$ adjacent towers in $\phi$, for a coverage of $(0.2 \times 0.2)$~in~$(\Delta \eta \times \Delta \phi)$~in the central region. Electromagnetic Calorimeter towers are also combined with Hadronic Calorimeter towers with similar $\eta$ and $\phi$.

These jet elements are sent to a jet-energy sum processor. This processor computes the event's scalar and missing transverse energy. The $\MET$ is computed from the negative vector sum of $\langle E_{\textrm{x}} , E_{\textrm{y}} \rangle$ of all jet elements. If the computed $\MET$ is above threshold, the event passes a \ac{L1} $\MET$ trigger. 

In order to cope with the high instantaneous luminosity delivered by the \ac{LHC} in Run~2, a pedestal correction is applied to the energy measurement of each jet element. This pedestal correction is computed for each bunch. The \ac{ATLAS} Liquid Argon Calorimeter signal must be integrated over $24$ bunch crossings ($600$ ns). This long readout window means that particles produced from collisions in neighboring bunches can produce energy depositions that will contribute to the signal. Collisions which are positioned at the beginning of a bunch train have a higher on average signal than collisions produced from the end of a bunch train. The average signal amplitude is referred to as the pedestal. The pedestal is computed for each bunch, at different instantaneously luminosities, and used to correct the energy of each jet element.

Figure~\ref{fig:l1_met_trigger_rate} shows how the pedestal correction helps to reduce the the $\MET$ trigger rate as a function of instantaneous luminosity. Because of this pedestal correction, \ac{ATLAS} was able to keep the threshold of the lowest unprescaled \ac{L1} $\MET$ trigger at $50$ GeV for all of Run~2. The correction is also applied to the Liquid Argon in the jet energy calibration used at \ac{HLT}.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Trigger/final_XE50ratesB_vs_instlumiB_50ns.pdf}
    \caption[Level 1 $\MET$ trigger rates]{The \ac{L1} $\MET$ trigger rate as a function of the instantaneous luminosity per bunch, for a threshold of $50$~GeV. The trigger rate is shown with and without the pedestal correction.~\cite{L1Calo}.}
    \label{fig:l1_met_trigger_rate}
  \end{center}
\end{figure}

For the \ac{HLT} $\MET$ trigger, one of the main challenges is finding a $\MET$ definition which is nearly consistent with the offline $\MET$ definition, and also robust to pile-up. Throughout the course of Run~2, many different $\MET$ definitions have been tested. In 2015, a cell-based approached was used as the default $\MET$ trigger. In 2016, a jet-based approach was used, while in 2017-2018 the default trigger used topological cluster-based algorithms with a pile-up subtraction scheme~\cite{METTrigger}. This analysis uses the the default $\MET$ triggers for data collected between 2016-2018, with the lowest unprescaled threshold. 

In 2016, the $\MET$ trigger used the jet-based \texttt{mht} definition, where \texttt{mht} stands for missing $H_{\textrm{T}}$. For a given event, the missing $H_{\textrm{T}}$ is the negative of the transverse momentum vector sum of all jets. Jets are reconstructed using the anti-$k_t$ jet finding algorithm from topological calorimeter clusters, (which are described in~\ref{sec:caloclusters}). These jets have a radius of $\Delta R= 0.4$, as well as pile-up subtraction and jet energy scale calibration applied.

In 2017-2018, the $\MET$ trigger used a pile-up fit, or \texttt{pufit}, algorithm. In the \texttt{pufit} method, the $\MET$ is calculated as the negative of the transverse momentum vector sum of all calorimeter topological clusters, corrected for pile-up. The pile-up correction is performed by grouping topological clusters into coarser towers of size $(0.7 \times 0.7)$ in $(\Delta \eta \times \Delta \phi)$. A pile-up-dependent threshold is used to separate towers into two categories. Towers with energies below the threshold are marked as pile-up, while towers with energies above the threshold are considered to be due to the $pp$-interaction of interest. 

A fit to below-threshold towers is performed in order to compute the event's transverse energy density due to pile-up. This fit takes tower energy resolutions and tower area in $\eta \times \phi$ into account. The fit assumes that pile-up makes no contribution to the event's $\MET$, and constrains the event-wide $\MET$ from below-threshold towers to zero, within resolution. This fit gives the pile-up transverse energy density. This fitted pile-up transverse energy density is used to correct towers which are above-threshold. %In later years, the thresholds for these algorithms were raised to compensate for increased pile-up. When thresholds were raised, lower trigger efficiencies were observed. 

Figure~\ref{fig:met_HLT_rates} shows the trigger cross section as a function of the mean number of interactions per bunch crossing, averaged over all bunch crossings in the \ac{LHC}. Data from 2016 are used to measure the cross section of the \texttt{mht} based trigger, while 2017 data are used to measure the cross section of the \texttt{pufit} based trigger. The trigger cross section is much larger for an \texttt{mht} based $\MET$ definition. Due to rate concerns, a \texttt{pufit} based $\MET$ definition was used for 2017 and 2018 data taking.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.6\textwidth]{plots/Trigger/MET_etxs_vs_mu_2017.pdf}
    \caption[$\MET$ High Level Trigger cross sections]{\ac{HLT} $\MET$ trigger cross section as a function of the mean number of $pp$-interactions per bunch crossing. The trigger cross section is shown for both the \texttt{pufit} and \texttt{mht} $\MET$ definitions~\cite{METTrigger}.}
    \label{fig:met_HLT_rates}
  \end{center}
\end{figure}

Figure~\ref{fig:met_trigger_eff} shows the $\MET$ trigger efficiency at \ac{L1} and \ac{HLT} as a function of the offline $\MET$ in $W\rightarrow\mu\nu$ events. The muon is not included in the offline $\MET$ definition, and treated as invisible. Events are collected with a single muon trigger. The \ac{L1} $\MET$ trigger reaches full efficiency by $\MET>180$~GeV, whereas the \ac{HLT} reaches full efficiency for events with $\MET>200$~GeV. The \texttt{pufit} method has a sharper turn on curve than the \texttt{mht} based $\MET$ triggers. High efficiency was maintained for events with $\MET > 200$~GeV throughout Run~2.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.8\textwidth]{plots/Trigger/dataWmunuEff_offlineMETNoMu.pdf}
    \caption[$\MET$ trigger efficiency]{The $\MET$ trigger efficiency at \ac{L1} and \ac{HLT} is shown as a function of offline $\MET$, computed for $W\rightarrow\mu\nu$ events, where the muon is ignored. The \ac{HLT} trigger efficiency is shown for both the \texttt{pufit} and \texttt{mht} definitions.~\cite{METTrigger}.}
    \label{fig:met_trigger_eff}
  \end{center}
\end{figure}

%Linearity of $\MET$ trigger is defined as the ratio of the $\MET$ measured by the trigger to the $\MET$ measured offline. The trigger $\MET$ resolution is worse than the offline $\MET$, which results in the slow turn-on in trigger efficiency. 

%{\color{red} Include some signal plots to show acceptance and efficiency of our signal in \MET Trigger}




