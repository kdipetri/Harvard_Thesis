\chapter{Large Hadron Collider}
\label{chp:largeHadronCollider}

%Possible references 
%http://cds.cern.ch/record/1997424 Radiofrequency cavities 
%A. W. Chao and M. Tigner, eds., Handbook of accelerator physics and engineering. 1999 
%The Large Hadron Collider , http://home.web.cern.ch/topics/large-hadron-collider,2015.
%LHC Study Group Collaboration, T. S. Pettersson and P. Lef`evre, The Large Hadron Collider Conceptual Design, Tech. Rep. CERN-AC-95-05-LHC, CERN, Oct, 1995. https://cds.cern.ch/record/291782.
%M. Bajko et al., Report of the Task Force on the Incident of 19th September 2008 at the LHC , Tech. Rep. CERN-LHC-PROJECT-Report-1168, CERN, Geneva, Mar, 2009. https://cds.cern.ch/record/1168025.
%R. Bruce, G. Arduini, S. Fartoukh, M. Giovannozzi, M. Lamont, et al., Baseline LHC machine parameters and configuration of the 2015 proton run, arXiv:1410.5990 [physics.acc-ph].
%The Accelerator Complex , tech. rep., CERN, 2015. http://home.web.cern.ch/about/accelerators.
%S. Myers, The Large Hadron Collider 2008-2013 , Int.J.Mod.Phys. A28 (2013) 1330035.
%H. Wiedemann, Particle Accelerator Physics. Springer, 2007.
%R. Bailey and P. Collier, Standard Filling Schemes for Various LHC Operation Modes, Tech. Rep. LHC-PROJECT-NOTE-323, CERN, Geneva, Sep, 2003.

%P. BryantA brief history and review of accelerators http://www.afhalifax.ca/magazine/wp-content/sciences/lhc/HistoireDesAccelerateurs/histoire1.pdf 

The Large Hadron Collider (\ac{LHC}) is a circular collider located on the border of Switzerland and France. The \ac{LHC} is $27$~km in circumference, and designed to accelerate two beams of protons to an energy of $7$~TeV. Proton beams are brought to collide head-on, at four points along the ring, producing collisions at a center-of-mass energy, $\sqrt{s}$ of $14$ TeV, and a peak instantaneous luminosity of $1.0 \times 10^{34}~\textrm{cm}^2\textrm{s}^-1$. Between 2015 and 2018, referred to as Run~2 of the \ac{LHC}, the \ac{LHC} provided $pp$-collisions at $\sqrt{s}=13$ TeV. The peak instantaneous luminosity was $2.1 \times 10^{34}~\textrm{cm}^2\textrm{s}^-1$, and the \ac{LHC} delivered $156~\ifb$ of integrated luminosity to the \ac{ATLAS} detector. 

%This chapter discusses why physicists use particle colliders to study fundamental interactions. The \ac{LHC} is circular collider, which can accelerate two beams of protons to an energy of $6.5$~TeV and cause them to collide head-on, creating center-of-mass energies of $13$ TeV. 

This chapter discusses how particle colliders are used to study fundamental interactions. Some basic information about particle accelerators is introduced in Section~\ref{sec:acceleratorBasics}, and Section~\ref{sec:lhcDesign} discusses specifics of the \ac{LHC} design. Operating parameters during Run~2 of the \ac{LHC} are described in Section~\ref{sec:LHCRun2}, and a discussion of the luminosity measurement is provided in Section~\ref{sec:luminosity}. More information about particle accelerators and colliders are discussed in References~\cite{Holzer:2203629,PhysRevD.98.030001}, and more details about the \ac{LHC} can be found in References \cite{Evans:2008zzb,Brüning:782076}.

\section{Accelerator Basics}\label{sec:acceleratorBasics}

Accelerators are used to accelerate particles to ultra-relativistic speeds and produce collisions at high energies. New heavy particles can be produced from high-energy collisions, as long as the energy of the interaction is greater than the rest mass of new hypothetical particle (allowed by $E=mc^2$). Accelerators can also be used as a microscope to probe small structures. The spatial resolution of the accelerator increases with particle energy according to the De Broglie relation $\lambda = \hbar/p$, where $\lambda$ is the De Broglie wavelength, which indicates the length scale, $\hbar$ is the Planck constant, and $p$ is the incoming particle momentum. In both cases, outgoing particles produced in these high energy collisions are studied in detectors.

%$E_{CM} = \sqrt{s}$ Mandelstam variable

Collisions are either produced by colliding a single beam with a fixed-target, or colliding two beams head-on. For the same beam energy, producing collisions with head-on beams results in center of mass energies twice that of a fixed-target experiment, and typically results in higher luminosities. In a fixed-target experiment it is also possible to produce a secondary beam of particles that may be stable or unstable, charged or neutral, whereas head-on collisions usually involve only stable charged particles (electrons, protons, or their anti-particles).   

There are two general accelerator designs for producing head-on collisions. Linear colliders consist of two identical halves. Each half accelerates a beam in a straight line, and collisions are produced at the center of the accelerator, where the two beams meet. Circular accelerators bend beams in two counter-rotating circles, collide the beams at fixed points along the circle. In circular colliders, the beam can be reused, usually resulting in larger integrated luminosity than at linear colliders.

The simplest way to accelerate particles is using a constant electric field. If the electric field is aligned in the particle's direction of motion, this method can accelerate particles up to a few MeV, but is limited by the highest voltage that can be applied. In order to achieve the high energies of interest to particle physicists, electric fields can be alternated, as is done in \ac{RF}-cavities~\cite{Radiofrequency:1997424}. %An \ac{RF}-cavity consists of a series of cylindrical electrodes, structured like beads on a string. Electrodes have holes in the middle, through which the beam is passed. 

The fields in \ac{RF}-cavities are made to alternate such that when a particle passes through a single cavity, and before it arrives at the next cavity, the electric fields are reversed. Reversing the electric fields ensures particles can only pass through the device in a single direction. Particles with slightly different energies, arriving earlier or later, will be accelerated or decelerated so that they stay close to the energy of the ideal particle. In this way, the particle beam is sorted into discrete packets called \textit{bunches}. The spacing in time between bunches ($25$ ns at the \ac{LHC}) is set by constraints of the detector, because the detector must be able to resolve individual bunches.  

%{\TODO include pic of RF cavity}

%In both linear and circular colliders, radio frequency cavities are used to accelerate beams. Circular colliders only require one such cavity, while linear accelerators require several in a long seriers to achieve high energies. Circular colliders also require magnets to bend the direction of the beam, while linear accelerators do not. 

In a circular collider, magnets are used to circulate the beam at a constant radius. The force, $\vec{f}$ a particle with charge $q$ and velocity, $\vec{v}$, experiences in a magnetic field, $\vec{B}$, is given by 
\begin{equation}
\vec{f} = q\vec{v} \times \vec{B}.
\end{equation}
Dipole magnets are used to create a magnetic field perpendicular to the velocity $\vec{v}$, while quadrupole magnets are used to focus the beam. The radius of circle is then

\begin{equation}
R = \frac{p}{q \cdot B}, 
\label{eqn:accelRadius}
\end{equation}
where $R$ is the radius of the accelerator. $p$ is the momentum perpendicular to the dipole magnetic field with strength $B$. If $B$ is expressed in Tesla, and $q=q_e$, as it does for protons and electrons, equation~\ref{eqn:accelRadius} can be reduced to solve for the energy of the beam in GeV as a function of the magnetic field strength and size of the accelerator. 
\begin{equation}
E_{\textrm{beam}} = 0.3 B \cdot R. 
\label{eqn:circularAccelEnergy}
\end{equation}

In Run~2 of the \ac{LHC}, beams reach a maximum energy of $6.5$~TeV. Given a radius of $27$~km, this means magnets that can produce a field of $\approx 7.5$~T are required. Fields this strong can only be produced by superconducting magnets operated at low temperatures. In iron, magnetic fields saturate at strengths between $1.6$ and $2.2$ T.  

Synchrotron radiation, the energy lost by relativistic charged particles due to acceleration, is another challenge for circular colliders. The energy loss due to the acceleration required to bend particles in a circular path is proportional to $\gamma^4$. Electrons have smaller mass than protons, which means that for the same energy electrons have a much larger $\gamma$. For circular colliders that accelerate electrons, synchrotron radiation is one of the major limiting considerations. In linear colliders, there is negligible loss of energy due to synchrotron radiation in comparison to circular colliders. 

There are other considerations which come into play when deciding between colliding electrons and protons. Electrons are fundamental particles, while protons are composite particles. At the energies produced in \ac{LHC}, collisions occur between the constituents of the proton, quarks and gluons (using the de Broglie relation $1$~TeV $\rightarrow 1.25\times10^{-18}$~m, whereas the size of a proton is $\approx 10^{-15}$~m). 

Because collisions occur between quarks and gluons, multiple production mechanisms are available at colliders which use protons. The fraction of proton momentum carried by these constituents isn't known \textit{a priori}, but must be measured, as \ac{PDF}s, which model the probability of finding a quark or gluon with a given momentum. These parton interactions produce collisions at a variety of energies, and the precise energy of any given collision is unknown. Interactions besides the hard scatter of interest often occur, and in rare cases, multiple partons from the same proton can produce two hard scatter collisions. The only quantity which is known, is the total momentum in the plane transverse to the beam, which must be zero. 

Another option is to collide protons with anti-protons. Anti-protons are harder to produce than protons, and generally limit the amount of integrated luminosity. However, there are several advantages to using anti-protons. Collisions with anti-protons result in different production mechanisms than a proton-proton collider, and protons and anti-protons can circulate in the same beam. 

At electron colliders, the energy of the collision is known, and momentum is balanced in the longitudinal plane. Because electrons are fundamental particles, there is no ambiguity in production mechanism of new particles. In general, electron colliders provide a much cleaner environment than hadron colliders. 

Another concern in accelerator design is the amount of data required to study interesting physics processes. For a given process, the number of predicted events in a dataset can be computed as

\begin{equation}
N_{\textrm{events}} = L_{\textrm{int}} \times \sigma_{\textrm{process}}, 
\end{equation}

where $L_{\textrm{int}}$ is the integrated luminosity of the dataset, and $\sigma_{\textrm{process}}$ is the cross section for the interesting physics process in question. 

Cross sections have dimensions of area, and are sometimes quoted in \textit{barn} ($b$), where 
\begin{equation}
1~\textrm{b} = 10^{-24}~\textrm{cm}^2.
\end{equation}

The integrated luminosity, is the integral of the instantaneous luminosity, $L$, with respect to time. 
\begin{equation}
L_{\textrm{int}} = \int L dt
\end{equation}

Cross sections for different physics processes can vary over many orders of magnitude. For example, the total inelastic cross section at $\sqrt{s}=13$~TeV is measured to be $\sigma_{\textrm{inel}}=(79.5\pm1.8)$~mb~\cite{Antchev:2296409}. The cross section for $t\bar{t}$ production, a relatively common Standard Model process, is predicted to be $\sigma_{t\bar{t}} = 831.76$~pb. In contrast, the cross section for pair-production of a $1.5$~TeV stop squark, the process studied in this thesis, is predicted to be $\sigma_{\tilde{t}\bar{\tilde{t} }} = 2.57 \times 10^{-4}$~pb. In the dataset considered by this analysis, which has an integrated luminosity of $136~\ifb$, this would correspond to nearly 35 predicted stop-antistop events. 

Obtaining the integrated luminosity necessary to study a given processes is a question of optimizing the instantaneous luminosity, because it is very difficult to increase the amount of time an accelerator can produce collisions in a given year. 

The instantaneous luminosity is the number of collisions that can be produced in a detector per $\textrm{cm}^2$ and per second, and can be computed as
\begin{equation}
L = \frac{ N_b^2~n_b~f_{\textrm{rev}}~\gamma }{ 4\pi~\sigma_x \sigma_y }~F = \frac{ N_b^2~n_b~f_{\textrm{ref}}~\gamma }{ 4\pi~\sqrt{ \epsilon_x \epsilon_y \beta_x \beta_y }}~F.
\end{equation}
The instantaneous luminosity is proportional to $N_b^2$, the number of protons per bunch ($\approx 10^{11}$ at the \ac{LHC}), as well as $n_b$, the number of bunches in each beam ($\approx 2500$), and $f_{\textrm{rev}}$, the frequency of revolution, or the number of times the beam circulates in one second ($\approx 1$ kHz). The instantaneous luminosity is inversely proportional to the root mean square, or rms, of the beam width in the $x$ and $y$ directions, $\sigma_x$ and $\sigma_y$. The emittance is denoted by $\epsilon$, while $\beta$ describes the transverse displacement of particles in the beam. The beta function changes as a function of position along the beam, and is referred to as $\beta \ast$  at the collision point. The term $F$ is a reduction factor due to the geometry of the beam crossing.

%The reduction factor is related to the crossing angle between colliding bunches $\theta_c$, and the rms of the beam width in the $z$ direction. It can be computed as 
%\begin{equation}
%F = (1 + ( \frac{ \theta_c ~\sigma_z}{ \sigma_{x} } )^2)^{−\frac{1}{2}}.
%\end{equation}

The instantaneous luminosity can be increased by increasing the number of bunches in the beam, increasing the number of protons per bunch, or reducing the rms spread of the particles in $x$ and $y$ directions at the interaction point. This step is referred to as \textit{squeezing} the beam, and achieved with focusing quadrupole magnets. 

During the course of a run, proton beams circulate and produce collisions. The number of protons per bunch, $N_b$, decreases due to beam losses, not due to collisions, producing a falling instantaneous luminosity. Runs continue for approximately ten hours at a time. The downtime between runs is usually a few hours, with extra breaks for scheduled and unscheduled maintenance for accelerator complex and detectors. There are also extended stops of several months each year, to upgrade detectors and the accelerator. In general, there are roughly ten weeks of data taking per year. 
 
In Run~2 of the \ac{LHC}, the peak instantaneous luminosity observed in \ac{ATLAS} was $2.1 \times 10^{34}~\textrm{cm}^2\textrm{s}^{-1}$, roughly twice the design instantaneous luminosity. One consequence of the high instantaneous luminosity at the \ac{LHC} is \textit{pile-up}, in which multiple collisions occur during the same bunch crossing, referred to as \textit{in-time} pile-up. Pile-up can also be \textit{out-of-time}, for detectors which are read out over intervals longer than the time between bunch crossings. For these detectors, it is important to associate signals to the correct beam-crossing. %Bunch cross spread is $200$~ps. 
%Average number of interactions per event was {\TODO XXX}.

Two important quantities are used to describe the amount of pile-up in a dataset. The first quantity is the number of primary vertices per event, $NPV$. The second is the average number of inelastic interactions per bunch crossing, $\langle \mu \rangle$, and can be computed as
\begin{equation}
\langle \mu \rangle = \frac{L \cdot \sigma_{\textrm{inelastic} } }{ N_{\textrm{bunch}} \cdot f_{\textrm{LHC}} }. 
\label{eqn:avgmu}
\end{equation}

The mean $\langle \mu \rangle$ of the full \ac{ATLAS} Run~2 dataset is approximately $33$ interactions per bunch crossing, and the peak $\langle \mu \rangle$ is approximately $70$ interactions per bunch crossing. There are many challenges for the \ac{ATLAS} experiment due to pile-up. In overcoming these challenges, it is vital to identify the correct primary vertex that produced an interaction of interest. With this information, it is possible to try to ignore other activity in the same bunch crossing, mainly jets from pile-up interactions, and only use particles associated with the interesting primary vertex. 

\section{Large Hadron Collider Design}\label{sec:lhcDesign}

%This section describes the design of the \ac{LHC}, including the injection chain,
%The \ac{LHC} accelerates two proton beams to $6.5$~TeV. These beams are brought to collide at four points around the ring, in the \ac{ATLAS}, \ac{CMS}, \ac{ALICE}, and \ac{LHCb} experiments. In order to 
%The \ac{LHC} was built to produce collisions at $\sqrt{s}=14$ TeV. Brief mention of the accident? 
The \ac{LHC} injection chain begins with a bottle of hydrogen gas located before Linac 2. An electric field is used to strip the hydrogen atoms of their electrons, producing protons. Linac 2 accelerates these protons to $50$~MeV, using a series of \ac{RF}-cavities. Protons are then transferred to the Proton Synchrotron Booster. The Proton Synchrotron Booster consists of four stacked synchrotron rings, and accelerates protons from $50$ MeV to $1.4$ GeV. Protons are then injected into the \ac{PS}. The Proton Synchrotron consists of $277$ iron electromagnets arranged along a circular beam path with a circumference of $628$ meters, and accelerates the protons up to $25$ GeV. The $25$~GeV protons are then injected into the \ac{SPS}. The \ac{SPS} is $7$ kilometers in circumference, and accelerates protons to $450$~GeV with conventional magnets. 

The \ac{SPS} then injects these protons into the \ac{LHC}, which accelerates protons to $6.5$~TeV. This process is achieved with eight \ac{RF} cavities, which increase the energy of the beam, and over $8000$ superconducting magnets, which bend the beam in a circle. 

The \ac{LHC}'s \ac{RF} cavities increase the energy of the beam from $450$ GeV after injection to $6.5$ TeV for collisions, more than $14$ times their injection energy. This process is referred to as \textit{ramping}. The \ac{LHC} radiofrequency cavities oscillate at a frequency of $400$~MHz. Each cavity can reach a maximum voltage of $2$ megavolts (MV), corresponding to $16$ MV in total, with an acceleration gradient of $5$ MV/m. Each time a proton circulates through the \ac{LHC} it receives $16$ MeV of energy. The maximum energy is achieved in around $20$ minutes, and corresponds to the bunches having passed through the \ac{RF} cavities more than $10$ million times. The \ac{RF} cavities also compensate for the $7$ keV of energy lost due to synchrotron radiation per beam per revolution.

The Large Hadron Collider contains over $8000$ niobium and titanium (NbTi) superconducting magnets. These magnets are cooled to $1.9$~K by liquid helium, which requires extensive cryogenics. Of the $8000$ superconducting magnets, $1232$ are the main bending dipoles, and the remaining magnets consist of quadrupoles, sextapoles, octopoles, and single bore dipoles. These magnets are used in focusing the beams, squeezing the beams before collisions, correcting trajectories, and damping oscillations in beam position. As the energy of the beam is increased, the current in the magnets is increased in order to keep the beam at fixed radius (following equation~\ref{eqn:accelRadius}).

The tunnel has a diameter of $3.7$ m, which makes it difficult to install two separate proton rings. To save space, the \ac{LHC} circulates both proton beams within the same cryostat, with separate magnetic fields and vacuum pipes for each beam. A cross section of an \ac{LHC} dipole is shown in Figure~\ref{fig:LHCdipole}

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/LHC/LHCdipole.jpg}
    \caption[Cross section of an LHC dipole magnet]{ A cross section of an \ac{LHC} dipole magnet~\cite{Team:40524}.}\label{fig:LHCdipole}
  \end{center}
\end{figure}

The \ac{LHC} beam is divided into $3654$ bunches, each separated by $25$ ns or $\approx 9.6$~m. The maximum allowed number of filled bunches in the \ac{LHC} is $2808$ and the maximum number of filled bunches in Run~2 was $2556$. Bunches are injected from the \ac{SPS} to the \ac{LHC} in \textit{bunch trains}. It is necessary to leave empty bunches between trains, due to the fill patterns of the previous accelerators, and in order to leave space to safely dump the beam. 

\section{Run~2 Operating Parameters}\label{sec:LHCRun2}

Table~\ref{tab:lhcOperatingParameters} shows the relevant operating parameters during Run~2. After a long shutdown, in 2015, the energy of the beam was increased from $4$~TeV to $6.5$~TeV, and bunch spacing was decreased from $50$ ns to $25$ ns. Increasing the energy in the beam required months of \textit{training} dipole magnets. A magnet is trained by increasing the current in the magnet until it quenches, and repeating the process over and over again until the maximum desired current is achieved. Reducing the spacing between bunches required overcoming electron-cloud and space-charge challenges, in which negatively charged electrons liberated from the accelerator walls are attracted to the positively-charged proton beam. The electron cloud was reduced by an intense period of \textit{scrubbing}. In order to improve the surface of the beam pipe, the \ac{LHC} circulated intense beams, containing many bunches, but at low energies. Due to the many challenges in commissioning, only $3.2~\ifb$ of data were collected in 2015.

\begin{table}[ht]
\begin{center}
\caption[LHC parameters during Run~2]{ \ac{LHC} parameters during Run~2, by year~\cite{LHCRun2Params}.}\label{tab:lhcOperatingParameters}
\begin{tabular}{ l c c c c c}

\hline	
\hline	
Parameter 									& Design	&	2015	&	2016	&	2017	& 	2018		\\
\hline
Energy 										& 7.0 		& 6.5		& 6.5		& 6.5		&	6.5			\\
Number of bunches 							& 2808		& 2244		& 2220		& 1868-2556	&	2556		\\	
Bunches per train 							& 288		& 144		& 96 		& 144-128	&	144			\\	
Energy per beam [MJ]						& 362		& 280		& 280		& 315		&	312			\\
$\beta\ast$ [cm] 							& 55		& 80		& 40		& 30-40		&	25-30		\\	
Protons per bunch [$10^{11}$] 				& 1.15		& 1.2		& 1.25		& 1.25		&	1.1			\\
Emittance [$\micron$] 						& 3.75		& 2.6-3.5 	& 1.8-2		& 1.8-2.2	&	1.8-2.2		\\		
Peak luminosity [$10^{34}$cm$^2$s$^{-1}$]	& 1.0		&  <0.6		& 1.5		& 2			&	2.1			\\
Half Crossing Angle [$\mu$rad] 				& 142.5		& 185 		& 140-185	& 120-150	&	130-150		\\	
\hline
\hline
\end{tabular}
\end{center}
\end{table}
%cite https://indico.cern.ch/event/751857/contributions/3259373/attachments/1783143/2910577/belen-Evian2019.pdf

The years 2016-2018 were production years for the \ac{LHC}. Each year, the \ac{LHC} performance was improved, and surpassed several design parameters. The peak instantaneous luminosity was more than twice design values, due to reductions in $\beta\ast$, increases in the number of protons per bunch, and the number of bunches per beam.

The \ac{LHC} performed so well that the \ac{ATLAS} and \ac{CMS} experiments had to use \textit{luminosity leveling} in order to cope with the rate of collisions. Luminosity leveling consists of adjusting the beam crossing angle, and separating the beams to reduce the effective cross section. In \ac{ATLAS}, the computing power available for the software-based High Level Trigger, which is described in Chapter~\ref{chp:trigger}, was the limiting factor in coping with instantaneous luminosity.

Figure~\ref{fig:integrateLumiVTime} shows the integrated luminosity accumulated over time. Between 2015 and 2018, the \ac{LHC} delivered $156~\ifb$ of $pp$-collisions to the \ac{ATLAS} detector. \ac{ATLAS} recorded $147~\ifb$ of data, and $139~\ifb$ are certified to have good data quality. This analysis makes use of $136~\ifb$ collected between 2016 and 2018.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/LHC/intlumivstime2018DQall.png}
    \caption[Total integrated luminosity in 2015-2018]{ Cumulative luminosity versus time delivered to \ac{ATLAS} (green), total integrated luminosity recorded by \ac{ATLAS} (yellow), and certified to be good quality data (blue) during stable beams for $pp$ collisions at $13$ TeV centre-of-mass energy in 2015-2018~\cite{Run2Lumi}.}\label{fig:integrateLumiVTime}
  \end{center}
\end{figure}

Figure~\ref{fig:averageMu} show the luminosity-weighted distribution of the mean number of interactions per bunch crossings for data collected between 2015 and 2018. The mean number of interactions per crossing is computed from equation~\ref{eqn:avgmu}. The integrated luminosity is computed from a preliminary luminosity calibration for $2018$, based on methods discussed in Section~\ref{sec:luminosity}. The average $\langle \mu \rangle$ is $33.7$ interactions per bunch crossing, and the peak $\langle \mu \rangle$ is approximately $70$ interactions per bunch crossing. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/LHC/mu_2015_2018.png}
    \caption[Mean number of interactions per bunch crossing for 2015-2018 data]{ Mean number of interactions per bunch crossing for $\sqrt{s}=13$~TeV $pp$-collision data collected between 2015-2018. All data recorded by \ac{ATLAS} is shown, and data collected during different calendar years are indicated by different colors. For each year, the mean $\langle \mu \rangle$ is provided in the legend~\cite{Run2Lumi}.}\label{fig:averageMu}
  \end{center}
\end{figure}

%Shown is the luminosity-weighted distribution of the mean number of interactions per crossing for the 2018 pp collision data at 13 TeV centre-of-mass energy. All data recorded by ATLAS during stable beams is shown, and the integrated luminosity and the mean mu value are given in the figure. The mean number of interactions per crossing corresponds to the mean of the poisson distribution of the number of interactions per crossing calculated for each bunch. It is calculated from the instantaneous per bunch luminosity as μ=Lbunch x σinel / fr where Lbunch is the per bunch instantaneous luminosity, σinel is the inelastic cross section which we take to be 80 mb for 13 TeV collisions, and fr is the LHC revolution frequency. The luminosity shown represents the preliminary 13 TeV luminosity calibration for 2018, released in February 2019, that is based on van-der-Meer beam-separation scans. Data collected by ATLAS for the entire 2018 run through the end of October are shown.

%{\TODO peak bunch crosses v time}
%Figure~\ref{fig:bunchCrossesVTime}

\section{Measuring Luminosity}\label{sec:luminosity}

%{\TODO needs a proofread and maybe a little expansion}

The \ac{ATLAS} detector measures the instantaneous and integrated luminosity delivered by the \ac{LHC} in a two step process, using methods similar used to those described in Reference~\cite{DAPR-2013-01}. 

First, dedicated sub-detectors measure the number of $pp$ inelastic collisions that occur while the \ac{LHC} is running. These measurements are performed online during data taking, and offline during the reconstruction step. Online luminosity measurements ensure the \ac{ATLAS} detector is efficiently collecting data (to adjust the trigger menu, and choose an appropriate luminosity leveling scheme) and ensure that the \ac{ATLAS} sub-detectors are protected from challenges that could arise during high instantaneous luminosity. Offline measurements are important for the most accurate determination of the size of the dataset. 
%Online, LUCID detector, hit AND. Offline pixel cluster counting. 

The second step calibrates the observed rate of $pp$-collisions, as measured in the first step, to a luminosity measurement. This step is performed with dedicated \textit{van der Meer} scans~\cite{vanderMeer:296752}. During a van der Meer scan, the beams begin by producing collisions head on. Then, one of the \ac{LHC} beams is moved in discrete steps the $x$ direction, until the hit rate drops to zero. This is designed to measure the profile of the beam. The same process is repeated in the $y$ direction in discrete steps.    






