\chapter{Results}
\label{chp:results}

\section{Final Yields}

The event yields in data are shown as a function of the various selections of the analysis. Table~\ref{tab:data_cutflow_met} shows the event cutflows in the $\MET$ triggered sample, while Table~\ref{tab:data_cutflow_muon} shows the event cutflow for the Muon triggered sample.

The first selection shown is the total number of events in the \verb=DAOD_SUSY15= derivations. The next selection shown is the number of events surviving n-tuple skimming. N-tuples were skimmed to require that each event have at least one baseline muon with $|d_0(\mu)|>1.0$ mm. For subsequent selections the relative and total efficiency is given with respect to the number of events surviving the n-tuple skimming.

Event level requirement requirements are applied first, followed by the muon selection and displaced vertex selections. The $\MET$ trigger sample primarily consists of events with a fake muon, and the muon preselection and full selection requirements are effective in rejecting these events. Only $58$ events pass the event selection and full muon selection in the $\MET$ trigger sample. In contrast, the Muon triggered sample primarily consists of events with a cosmic muon. There are $108147$ events which survive the event selection and all muon selection criteria except for the cosmic veto, highlighting the size of the cosmic muon background and importance of the cosmic veto. After the cosmic veto is applied, there only $452$ events in the Muon triggered sample. 

In the $\MET$ triggered sample, most events which pass the muon selection also have a displaced vertex, while in the Muon triggered sample, fewer than half of the events also have a displaced vertex. First, fiducial and quality requirements are applied to displaced vertices, followed by the Material Veto. The Material Veto is the last \ac{DV} preselection requirement applied, in order to understand the contribution of hadronic interactions. In both the Muon and $\MET$ triggered samples, roughly $35\%$ of displaced vertices in the fiducial volume are rejected by the Material Veto. 

In both the $\MET$ and Muon triggered samples, the full displaced vertex requirements are effective in reducing the final yields to only a handful of events. After the Material Veto is applied, displaced vertices are dominated by randomly crossing tracks. The requirement that displaced vertices have at least three tracks rejects between $85\%$ and $90\%$ of displaced vertices which survive a Material Veto. In the $\MET$ triggered sample, no events survive the full selection. In the Muon triggered sample, only one event survives the full event selection.  

\begin{table}[!ht]
\begin{center}
\caption[$\MET$ triggered sample cutflows in data]{$\MET$ triggered sample cutflows in data. The first selection shown is the total number of events in the \ac{SUSY} derivations. The next selection shown is the number of events surviving n-tuple skimming. N-tuples were skimmed to require that each event have at least one baseline muon with $|d_0(\mu)| > 1.0$ mm. For subsequent selections the relative and total efficiency is given with respect to the number of events surviving the n-tuple skimming.}\label{tab:data_cutflow_met}
\begin{adjustbox}{max width=0.95\textwidth}
 \begin{tabular}{lccc}
  \hline\hline

   Cut & Events & Rel. Efficiency [$\%$] & Total Efficiency [$\%$]\\
   \hline
   Derivation & $564932150$ &  &  \\
   N-tuple skimming & $2703349$ & $100.0$ & $100.0$ \\
   \input{plots/Cutflows/SR_MET_data.tex}

  \hline\hline
 \end{tabular}
\end{adjustbox}
\end{center}
\end{table}


\begin{table}[!ht]
\begin{center}
\caption[Muon triggered sample cutflows in data]{Muon triggered sample cutflows in data. The first selection shown is the total number of events in the \ac{SUSY} derivations. The next selection shown is the number of events surviving n-tuple skimming. N-tuples were skimmed to require that each event have at least one baseline muon with $|d_0(\mu)| > 1.0$ mm. For subsequent selections the relative and total efficiency is given with respect to the number of events surviving the n-tuple skimming. }\label{tab:data_cutflow_muon}
\begin{adjustbox}{max width=0.95\textwidth}
 \begin{tabular}{lcccccc}
  \hline\hline

   Cut & Events & Rel. Efficiency [$\%$] & Total Efficiency [$\%$]\\
   \hline
   Derivation & $564932150$ &  &  \\
   N-tuple skimming & $2703349$ & $100.0$ & $100.0$ \\
   \input{plots/Cutflows/SR_MU_data.tex}

  \hline\hline
 \end{tabular}
\end{adjustbox}
\end{center}
\end{table}

\clearpage
\section{Signal Acceptance and Efficiency}

Results are interpreted in the context of \RP violating Supersymmetry model discussed in Section~\ref{sec:simplifiedModel}. In this simplified model, long-lived stop particles are pair produced in $pp$-collisions. In this model, the stop is the lightest \ac{SUSY} particle, and decays via a non-zero but small $\lamp_{23k}$ coupling to a muon and quark. %Several characteristic distributions of the signal can be found in Appendix~\ref{app:stopSignalKinematics}.

The number of events selected, and total selection efficiency for the $\MET$ triggered sample are shown in Table~\ref{tab:signal_cutflow_met} for stops with $m(\tilde{t})=1.4$ TeV, and three lifetimes. Figure~\ref{fig:met_signal_total_events_efficiency} also shows the expected number of stop events expected to pass the full selection as well as the total signal acceptance times efficiency in the $\MET$ triggered sample, as a function of $m(\tilde{t})$ and $\tau(\tilde{t})$, for $136$ fb$^{-1}$ of integrated luminosity. 

The total acceptance times efficiency for this signal model ranges from $33\%$ to $35\%$ for stops with lifetime $\tau(\tilde{t})=0.1$ ns, with little dependence on stop mass. For stops with lifetime $\tau(\tilde{t}) = 1$ ns, the total acceptance times efficiency ranges from $15\%$ to $16\%$, while for stops with lifetime $\tau(\tilde{t}) = 0.01$ ns the total acceptance times efficiency ranges from $4\%$ to $6\%$.

%{\color{red} Using NLO+NLL cross sections. To be updated to NNLO+NNLL.}

\begin{table}[!ht]
\begin{center}
\caption[$\MET$ triggered sample cutflows for signal]{$\MET$ triggered sample cutflows for signal samples with $m(\tilde{t}) = 1.4$ TeV, and $\tau(\tilde{t}) = 0.01$, $0.1$, and $1$ ns.}\label{tab:signal_cutflow_met}
\begin{adjustbox}{max width=0.95\textwidth}
 \begin{tabular}{lcccccc}

\hline\hline
      & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~} & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~}  & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~}   \\
      & \multicolumn{2}{c}{~$\tau(\tilde{t})=0.01$ ns~} & \multicolumn{2}{c}{~$\tau(\tilde{t})=0.1$ ns~}  & \multicolumn{2}{c}{~$\tau(\tilde{t})=1$ ns~}   \\
\hline
          &        & Overall     &        & Overall     &         & Overall    \\
Selection & Events & Efficiency  & Events & Efficiency  & Events  & Efficiency \\
          &        & [$\%$]      &        & [$\%$]      &         & [$\%$]\\
\hline
\input{plots/Cutflows/SR_MET_rhad_1400_v_lifetime.tex}
  \hline\hline
 \end{tabular}
\end{adjustbox}
\end{center}
\end{table}

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Cutflows/2D_SRMET_Events.pdf}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Cutflows/2D_SRMET_Efficiency.pdf}
    \caption[$\MET$ triggered sample expected signal yields]{$\MET$ triggered sample expected signal yields, for three lifetimes and stop masses between $1$ and $2$ TeV. Left: The number of stop events expected to pass the full selection as a function of $m(\tilde{t})$ and $\tau(\tilde{t})$ Right: The total signal efficiency for selecting stop events as a function of $m(\tilde{t})$ vs. $\tau(\tilde{t})$.}
    \label{fig:met_signal_total_events_efficiency}
  \end{center}
\end{figure}

The Muon triggered signal selection efficiency and expected number of events for stops with $m(\tilde{t})=1.4$ TeV, and three lifetimes are shown in Table ~\ref{tab:signal_cutflow_muon}. Figure~\ref{fig:muon_signal_total_events_efficiency} also shows the total number of stop events expected to pass the full Muon triggered selection, as well as the total signal acceptance times efficiency in the Muon triggered sample as a function of stop mass and lifetime. Most signal events produce very high-$\pT$ muons, which are not back-to-back in $\phi$. The resulting large cluster-based \MET means that most signal events entering the $\MET$ triggered sample. Though the Muon triggered sample has very low efficiency for the $\tilde{t} \rightarrow \mu + j$ signals, it is used to retain sensitivity to other potential signals which might produce lower $\pT$, or fewer, muons.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Cutflows/2D_SRMU_Events.pdf}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/Cutflows/2D_SRMU_Efficiency.pdf}
    \caption[Muon triggered sample expected signal yields]{Muon triggered sample expected signal yields, for three lifetimes and stop masses between $1$ and $2$ TeV. Left: The number of stop events expected to pass the full selection as a function of $m(\tilde{t})$ and $\tau(\tilde{t})$ Right: The total signal efficiency for selecting stop events as a function of $m(\tilde{t})$ vs. $\tau(\tilde{t})$}
    \label{fig:muon_signal_total_events_efficiency}
  \end{center}
\end{figure}


\begin{table}[!ht]
\begin{center}
\caption[Muon triggered sample cutflows for signal]{Muon triggered sample cutflows for signal samples with $m(\tilde{t}) = 1.4$ TeV, and $\tau(\tilde{t}) = 0.1$ and $1$ ns.}\label{tab:signal_cutflow_muon}
\begin{adjustbox}{max width=0.95\textwidth}
 \begin{tabular}{lcccccc}

\hline\hline
      & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~} & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~}  & \multicolumn{2}{c}{~$m(\tilde{t})=1.4$ TeV,~}   \\
      & \multicolumn{2}{c}{~$\tau(\tilde{t})=0.01$ ns~} & \multicolumn{2}{c}{~$\tau(\tilde{t})=0.1$ ns~}  & \multicolumn{2}{c}{~$\tau(\tilde{t})=1$ ns~}   \\
\hline
          &        & Overall     &        & Overall     &         & Overall    \\
Selection & Events & Efficiency  & Events & Efficiency  & Events  & Efficiency \\
          &        & [$\%$]      &        & [$\%$]      &         & [$\%$]\\
\hline
\input{plots/Cutflows/SR_MU_rhad_1400_v_lifetime.tex}

  \hline\hline
 \end{tabular}
\end{adjustbox}
\end{center}
\end{table}


For $\tilde{t} \rightarrow \mu + j$ signals, the analysis sensitivity depends largely on the stop particle lifetime. For particles with shorter lifetimes, for example $\tau = 0.01$ ns, most long-lived particles decay too close to the interaction point to pass the requirement that secondary vertices be displaced from any primary vertex in the event by more than $4$ mm in the transverse plane. As seen in Figure~\ref{fig:long_lived_particle_decay_position_rxy_z}, only $10\%$ of stop particles with $\tau(\tilde{t}) = 0.01$ ns and $m(\tilde{t}) = 1$ TeV would decay inside the fiducial volume of $4 < R_{xy} < 300$ mm and $|z|<300$ mm.

In contrast, signals with longer lifetimes produce more decays past $R_{xy} > 4$ mm. For example $71\%$ of decays from stops with $\tau(\tilde{t}) = 0.1$ ns and $m(\tilde{t}) = 1$ TeV, and $70\%$ of decays from stops with $\tau(\tilde{t}) = 1$ ns and $m(\tilde{t}) = 1$ TeV occur in the same fiducial fiducial volume of $4 < R_{xy} < 300$ mm and $|z|<300$ mm.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/VertexAcceptance/Acceptance_vtx_rxy.pdf}
    \includegraphics[angle=-90,width=0.48\textwidth]{plots/VertexAcceptance/Acceptance_vtx_z.pdf}
    \caption[$R$-hadron decay position in $R_{xy}$ and $z$]{Left: The $R$-hadron decay position in $R_{xy}$ for stop particles with $m(\tilde{t})=1$ TeV, and different lifetimes. Right: The $R$-hadron decay position decay position in $z$ for stop particles with $m(\tilde{t})=1$ TeV, and different lifetimes.}
    \label{fig:long_lived_particle_decay_position_rxy_z}
  \end{center}
\end{figure}

Despite having similar acceptance, the analysis is more sensitive to stop particles with lifetime $\tau(\tilde{t}) = 0.1$~ns than $\tau(\tilde{t}) = 1$~ns. This difference in sensitivity is due to reduced reconstruction efficiency for decays with larger radial displacement. Because tracking efficiency degrades as a function of radial displacement, fewer muons are likely to be reconstructed if $\tau(\tilde{t}) = 1$~ns, and there are also fewer tracks available to form vertices at large radii. Secondary vertex reconstruction efficiency also degrades as a function of displacement, even after accounting for reduced tracking efficiency. The effect of the Material Veto on signal efficiency is also different for different lifetimes. For stop particles with $\tau(\tilde{t}) = 0.1$~ns, most decays occur at radii before beam-pipe and first Pixel layer. In contrast, stop particles with $\tau(\tilde{t}) = 1$~ns do decay at radii past the first Pixel layer, and $25\%$ of vertices are rejected by the Material Veto.

\begin{table}[!ht]
\begin{center}
\caption[Vertex level acceptance and efficiencies]{Vertex Level Acceptance and Efficiencies for $m(\tilde{t}) = 1$ TeV, and $\tau(\tilde{t}) = 0.1$ and $1$ ns}
\begin{adjustbox}{max width=0.95\textwidth}
 \begin{tabular}{lccc}

\hline\hline
      & $m(\tilde{t})=1.4$ TeV,~    & ~$m(\tilde{t})=1.4$ TeV,~  & ~$m(\tilde{t})=1.4$ TeV,~  \\
      & ~$\tau(\tilde{t})=0.01$ ns~ & ~$\tau(\tilde{t})=0.1$ ns~ & ~$\tau(\tilde{t})=1$ ns~   \\[1ex]
\hline
\textit{Acceptance}    &          &           &         \\
~DV Fiducial Volume    &   0.11   &   0.71    &   0.70  \\
~DV-PV Distance        &          &           &         \\
\hline
\textit{Efficiency}    &          &           &         \\
~Reco Matched          &          &           &         \\
~$\chi^2/N_{dof} < 5$  &   0.64   &   0.63    &   0.28  \\
~$\nTracks \geq 3$     &          &           &         \\
~$\mDV > 20$ GeV       &          &           &         \\
\hline
\textbf{Material Veto} &   1.00   &   0.90    &   0.75  \\
\hline
\textbf{Total Vertex Efficiency} & 0.07  &   0.40    &   0.15  \\
\hline\hline&

 \end{tabular}
\end{adjustbox}
\end{center}
\end{table}

The analysis sensitivity does not greatly change as a function of stop particle mass. The reason for this effect is that decay kinematics do not significantly change as a result of the stop mass. For the masses considered by this analysis, the stop will always be a heavy object undergoing a two-body decay. There are two minor competing effects in terms of signal acceptance and efficiency as the stop mass increases. Stop particles with larger mass tend to result in higher momentum muons and tracks. The higher momentum muons produce events with larger cluster-based $\MET$, increasing the analysis sensitivity. However, higher momentum tracks, which are separated at a large angle result in slightly poorer vertex reconstruction efficiency. For the signal considered here, these two effects are small (order $1\%$) compared to the more substantial changes in sensitivity to lifetime.


\section{Distributions}

Figure~\ref{fig:stacked_backgrounds_dv_ntracks} shows the track multiplicity for displaced vertices in events which have at least one fully selected muon and at least one displaced vertex passing preselection. In events which have multiple displaced vertices passing selection, all vertices are shown. The background predictions for events with cosmic, fake, and heavy-flavor muons are shown as filled histograms. Vertices observed in data are shown with black markers. Signal predictions are overlaid with dashed lines. There is good agreement between vertices observed in data, and the background prediction.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plots/StackedBackgrounds/fig_02a.png}
    \includegraphics[width=0.48\textwidth]{plots/StackedBackgrounds/fig_02b.png}
    \caption[Displaced vertex track multiplicity]{Displaced vertex track multiplicity. Events are required to have at least one fully selected muon, and at least one displaced vertex which passes preselection. Vertices observed in data are shown with black points. The heavy-flavor, fake, and cosmic muon backgrounds are shown as filled histograms. The statistical and systematic uncertainty on the total predicted background is shown with hashed lines. Signal predictions are overlaid with dashed lines. The requirement that $\nTracks \geq 3$ is visualized with a black arrow. Left: $\MET$ triggered sample. Right: Muon triggered sample~\cite{ATLAS-CONF-2019-006}.}
    \label{fig:stacked_backgrounds_dv_ntracks}
  \end{center}
\end{figure}

Figure~\ref{fig:stacked_backgrounds_dv_mass} shows the displaced vertex mass for events which have at least one fully selected muon and at least one displaced vertex with at least three tracks which passes preselection. In events which have multiple displaced vertices passing selection, only the highest mass displaced vertex is shown. The background predictions for events with cosmic, fake, and heavy-flavor muons are shown as filled histograms. Vertices observed in data are shown with black markers. Signal predictions are overlaid with dashed lines. There is good agreement between vertices observed in data, and the background prediction.

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plots/StackedBackgrounds/fig_02c.png}
    \includegraphics[width=0.48\textwidth]{plots/StackedBackgrounds/fig_02d.png}
    \caption[Displaced vertex mass for vertices with at least three tracks]{Displaced vertex mass. Events are required to have at least one fully selected muon, and at least one displaced vertex which passes preselection with three or more associated tracks. Only the displaced vertex with the largest invariant mass is shown per event. Vertices observed in data are shown with black points. The heavy-flavor, fake, and cosmic muon backgrounds are shown as filled histograms. The statistical and systematic uncertainty on the total predicted background is shown with hashed lines. Signal predictions are overlaid with dashed lines. The requirement that $\mDV > 20$~GeV is visualized with a black arrow. Left: $\MET$ triggered sample. Right Muon triggered sample~\cite{ATLAS-CONF-2019-006}.}
    \label{fig:stacked_backgrounds_dv_mass}
  \end{center}
\end{figure}

Cutflows for the $\MET$ triggered sample and Muon triggered sample are visualized in Figure~\ref{fig:cutflow}. Cutflows are shown for event yields in data and for three example signal points. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plots/Cutflows/fig_03a.png}
    \includegraphics[width=0.48\textwidth]{plots/Cutflows/fig_03b.png}
    \caption[Visualized cutflows]{The cutflow is visualized for the $\MET$ triggered sample (left) and the Muon triggered sample (right) in data and for three example signal points. The first selection shown, referred to as the Initial Filter, is the total number of events events surviving n-tuple skimming. N-tuples were skimmed to require that each event have at least one baseline muon with $|d_0(\mu)| > 1.0$ mm. For subsequent selections the relative and total efficiency is given with respect to the number of events surviving the n-tuple skimming. Muon and displaced vertex preselection criteria are combined into a single point.}
    \label{fig:cutflow}
  \end{center}
\end{figure}




\clearpage


\section{Physics Interpretation}
\subsection{Simplified Model Interpretation}

The expected and observed 95\% CL exclusion limits on the mass $m(\tilde{t})$ of a long-lived stop squark as a function of its mean proper lifetime $\tau(\tilde{t})$ are shown in Figure~\ref{fig:expected_results}. For the stop masses and lifetimes considered, the $\MET$ trigger Signal Region provides the best sensitivity. Contours of constant values of $\lambda^{\prime}_{32k}\cos{\theta_{t}}$ are also shown where $\theta_t$ is the mixing angle between the right- and left-handed stop squarks. 


\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Interpretation/fig_05a.png}
    \caption[Observed and expected exclusions]{Observed and expected exclusion limits at 95\% CL on $m(\tilde{t})$ as a function of $\tau(\tilde{t})$ are shown along with contours showing fixed values of $\lambda'_{23k}\cos \theta_t$. Results are interpreted in terms of di-stop production, where the stop decays via a $\lambda'_{23k}$ \RP violating coupling. Experimental and theoretical uncertainties are shown.~\cite{ATLAS-CONF-2019-006}.
    }
    \label{fig:expected_results}
  \end{center}
\end{figure}

%Signal and background systematic uncertainties discussed in this text are included. The signal systematic uncertainties are added in quadrature and given a single nuisance parameter in the fit. Limits are computed assuming NNLO+NNLL signal cross sections. Figure~\ref{fig:expected_results_CLs} additionally shows the underlying $CL_S$ values as grey numbers. Expected 95\% CL upper limits on the cross section are shown in Figure~\ref{fig:XSExclusions} as a function of $m(\tilde{t})$ for several values of $\tau(\tilde{t})$. 
For a mean proper lifetime of $\tau(\tilde{t})=0.1$ ns, masses below roughly  $m(\tilde{t})=1.7$~TeV are excluded. For the range of mean proper lifetimes between $\tau(\tilde{t})=0.01$~ns and $\tau(\tilde{t})=30$~ns, masses below $1.3$~TeV are excluded. For $m(\tilde{t})=1$~TeV, values of $\lambda'_{23k} \cos \theta_t$ between roughly $10^{-8}$ and $10^{-10}$ are excluded at 95\% CL. 


Figure~\ref{fig:XSExclusions} shows the 95\% CL upper limits on the production cross section of these signals as a function of $m(\tilde{t})$ for various values of $\tau(\tilde{t})$. For a mean proper lifetime of $\tau(\tilde{t})=0.1$~ns, cross section upper limits are set below $100$~ab. For mean proper lifetimes between $\tau(\tilde{t})=0.01$~ns and $\tau(\tilde{t})=100$~ns, these limits are the strictest to date on models with a metastable stop squark decaying via a $\lambda'_{ijk}$ \RP violating coupling. 

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{plots/Interpretation/fig_05b.png}
    \caption[Cross section upper limits]{Cross section upper limits are shown as a function of $m(\tilde{t})$ for several values of $\tau(\tilde{t})$. Results are interpreted in terms of di-stop production, where the stop decays via a $\lambda'_{23k}$ \RP violating coupling. The nominal signal production cross section and its theoretical uncertainty are also indicated~\cite{ATLAS-CONF-2019-006}}
    \label{fig:XSExclusions}
  \end{center}
\end{figure}

%Figure~\ref{fig:expected_results_CLs_wCMS} additionally shows a recent exclusion from \ac{CMS} for this model.

%{\TODO lifetime reweighting reference or explanation}

%{\color{blue}Signal sensitivity fits are evaluated using the $CL_s$ prescription as implemented in the common fitting package \texttt{HistFitter} with commit:

%\texttt{221b6c8d76f27b3d4c3e53f72e4cc852eed751eb}.

%Contour creation was performed with the command:

%\texttt{harvestToContours.py -i DVLimits\_Output\_fixSigXSecNominal\_hypotest\_\_1\_harvest\_list.json -o outputGraphs.root -x tau -y mstop --logX -d -l None --interpolation multiquadric -s 0.1 --interpolationEpsilon 2}

%}

\clearpage

\subsection{Model-Independent Limits}

Model-independent 95\% CL cross section limits are also obtained. At 95\% CL, the $\MET$ triggered signal region excludes visible \ac{BSM} cross sections above $0.02$~fb while the Muon triggered signal excludes visible \ac{BSM} cross sections above $0.03$~fb. The results of these model-independent fits can be found in Table~\ref{table.results.exclxsec.pval.upperlimit.SS}.
%, finding the upper limit on a floating 1-event dummy signal. 


%using the \texttt{HistFitter} prescription
%{\color{blue}With 10,000 toys, the \texttt{HistFitter} \texttt{UpperLimitTable.py} script is used on dedicated RooStats workspaces.}

% {\color{red}Fits with 50,000 toys are being performed to ensure 10,000 toys are sufficient to converge on a stable limit.}

\begin{table}
\centering
\setlength{\tabcolsep}{0.0pc}
\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lccccc}
\noalign{\smallskip}\hline\noalign{\smallskip}
Signal Region                        & $\langle\epsilon{\sigma }\rangle_{\textrm{obs} }^{95}$[fb]  &  $S_{\textrm{obs} }^{95}$  & $S_{\textrm{exp} }^{95}$ & $CL_{B}$ & $p(s=0)$  \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%


 $\MET$ triggered SR    & $0.02$ &  $3.1$ & $ { 3.1 }^{ +1.1 }_{ -0.1 }$ & $0.12$ & $ 0.50$ \\%
% SS    & $0.02$ &  $3.1$ & $ { 3.0 }^{ +1.1 }_{ -0.0 }$ & $0.12$ & $ 0.62$~$(-0.31)$ \\% with 50k toys
 Muon triggered SR    & $0.03$ &  $3.7$ & $ { 4.2 }^{ +1.6 }_{ -1.0 }$ & $0.28$ & $ 0.50$ \\%


\noalign{\smallskip}\hline\noalign{\smallskip}
\end{tabular*}
\caption[Breakdown of model-independent upper limits]{
Left to right: 95\% CL upper limits on the visible cross section
($\langle\epsilon\sigma\rangle_{\textrm{obs} }^{95}$) and on the number of
signal events ($S_{\textrm{obs} }^{95}$ ).  The third column
($S_{\textrm{exp} }^{95}$) shows the 95\% CL upper limit on the number of
signal events, given the expected number (and $\pm 1\sigma$
excursions on the expectation) of background events.
The last two columns
indicate the $CL_B$ value, i.e. the confidence level observed for
the background-only hypothesis, and the discovery $p$-value ($p(s = 0)$). 
\label{table.results.exclxsec.pval.upperlimit.SS}}
\end{table}
%
%







