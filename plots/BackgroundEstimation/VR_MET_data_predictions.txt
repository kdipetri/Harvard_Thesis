DV Selection & Cosmic & Fake & HeavyFlavor & Total Pred. & Total Obs. \\ 
No DVs & 	0.44 &	6.93 &	11.50 &	18.87  & 28  \\ 
Mat 2 Track & 	0.09 &	4.32 &	8.71 &	13.12  & 16  \\ 
Mat 3 Track & 	0.00 &	1.07 &	2.96 &	4.03  & 6  \\ 
2 Track DVs & 	0.03 &	13.82 &	20.03 &	33.89  & 30  \\ 
3 Track LM & 	0.01 &	3.21 &	4.18 &	7.40  & 6  \\ 
3 Track HM & 	0.00 &	0.32 &	0.35 &	0.67  & 0  \\ 
